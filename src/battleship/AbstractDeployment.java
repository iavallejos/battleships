package battleship;

/**
 * Class where the deployment rules are checked.
 * 
 * @author Ignacio Vallejos.
 *
 */
public abstract class AbstractDeployment implements IDeployment {

  @Override
  public boolean deployable(int x0, int y0, int xf, int yf, Territory board) {
    if (outOfGrid(x0, y0, xf, yf, board) || isDiagonal(x0, y0, xf, yf)) {
      return false;
    } else if (deploymentRule(x0, y0, xf, yf, board)) {
      return true;
    }
    return false;
  }

  /**
   * Checks if one of the points from (x0,y0) to (xf,yf) are outside the grid and if the final.
   * points coordinates are smallest than the initial ones.
   * 
   * @param x0 initial x coordinate for the ship.
   * @param y0 initial y coordinate for the ship.
   * @param xf final x coordinate for the ship.
   * @param yf final y coordinate for the ship.
   * @return true if any of the coordinates are outside the grid, false if they are inside it.
   */
  boolean outOfGrid(int x0, int y0, int xf, int yf, Territory board) {
    return xf > board.getWidth() || x0 < 1 || yf > board.getHeight() || y0 < 1 || xf < x0
        || yf < y0;
  }

  /**
   * Checks if the points from (x0,y0) to (xf,yf) represents an diagonal line.
   * 
   * @param x0 initial x coordinate for the ship.
   * @param y0 initial y coordinate for the ship.
   * @param xf final x coordinate for the ship.
   * @param yf final y coordinate for the ship.
   * @return true if the line is diagonal.
   */
  boolean isDiagonal(int x0, int y0, int xf, int yf) {
    return !(x0 == xf || y0 == yf);
  }

  /**
   * Checks the deployment rules of the selected Deploy for a ship between the points (x0,y0) and.
   * (xf,yf).
   * 
   * @param x0 initial x coordinate for the ship.
   * @param y0 initial y coordinate for the ship.
   * @param xf final x coordinate for the ship.
   * @param yf final y coordinate for the ship.
   * @param territory The Territory to check.
   * @return true if the ship can be deployed and false if it can't.
   */
  abstract boolean deploymentRule(int x0, int y0, int xf, int yf, Territory territory);
}
