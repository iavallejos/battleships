package battleship;

import battleship.attack.IAttackResult;
import battleship.attack.SuccessfulAttack;
import battleship.attack.UnsuccessfulAttack;
import battleship.display.CompactDisplay;
import battleship.display.DesktopDisplay;
import battleship.display.IDesktopDisplay;
import battleship.display.ReverseDesktopDisplay;
import battleship.display.SpaceDisplay;
import battleship.display.TildeDisplay;
import battleship.ship.IShip;

import java.util.Iterator;


/**
 * Battleship class is a facade class, it uses Territory, Fleet, IDisplay, IDeploiment and IPosition
 * for its methods, it represents an Battleship game board.
 * 
 * @author Ignacio Vallejos.
 *
 */
public class Battleship {
  public final Player playerOne = new Player();
  public final Player playerTwo = new Player();
  private IDesktopDisplay desktopDisplay = new DesktopDisplay();

  /**
   * Adjusts the size of the territory.
   * 
   * @param width the desired width of the territory.
   * @param height the desired height of the territory.
   */
  public void setTerritorySize(int width, int height) {
    playerOne.setTerritorySize(width, height);
    playerTwo.setTerritorySize(width, height);
  }

  /**
   * Sets the name of the player One.
   * 
   * @param name The name to be set.
   */
  public void setPlayerOneName(String name) {
    playerOne.setName(name);
  }

  /**
   * Sets the name for the player Two.
   * 
   * @param name The name to be set.
   */
  public void setPlayerTwoName(String name) {
    playerTwo.setName(name);
  }

  /**
   * @return The height of the territory.
   */
  public int getTerritoryHeight() {
    return playerOne.getTerritoryHeight();
  }

  /**
   * @return The width of the territory.
   */
  public int getTerritoryWidth() {
    return playerOne.getTerritoryWidth();
  }

  /**
   * @return The name of the player One.
   */
  public String getPlayerOneName() {
    return playerOne.getName();
  }

  /**
   * @return The name of the player Two.
   */
  public String getPlayerTwoName() {
    return playerTwo.getName();
  }

  /**
   * Sets the display for the two players to Tilde.
   */
  public void useTildeDisplay() {
    playerOne.setDisplay(new TildeDisplay());
    playerTwo.setDisplay(new TildeDisplay());
  }

  /**
   * Sets the display for the two players to Space.
   */
  public void useSpaceDisplay() {
    playerOne.setDisplay(new SpaceDisplay());
    playerTwo.setDisplay(new SpaceDisplay());
  }

  /**
   * Sets the desktop display of the game to CompactDisplay.
   */
  public void useCompactDisplay() {
    desktopDisplay = new CompactDisplay();
  }

  /**
   * Sets the desktop display of the game to DesktopDisplay.
   */
  public void useDesktopDisplay() {
    desktopDisplay = new DesktopDisplay();
  }

  /**
   * Sets the desktop display of the game to ReverseDesktopDisplay.
   */
  public void useReverseDesktopDisplay() {
    desktopDisplay = new ReverseDesktopDisplay();
  }

  /**
   * @return A string that represents the territory of the player One drawn according to the
   *         established display.
   */
  public String displayPlayerOneTerritory() {
    return playerOne.displayTerritory();
  }

  /**
   * @return A string that represents the territory of the player Two drawn according to the
   *         established display.
   */
  public String displayPlayerTwoTerritory() {
    return playerTwo.displayTerritory();
  }

  /**
   * @return A String that represents the Desktop of the player One acoording to the stablished
   *         DesktopDisplay.
   */
  public String displayPlayerOneDesk() {
    return desktopDisplay.displayDesktop(playerOne, playerTwo);
  }

  /**
   * @return A String that represents the Desktop of the player Two acoording to the stablished
   *         DesktopDisplay.
   */
  public String displayPlayerTwoDesk() {
    return desktopDisplay.displayDesktop(playerTwo, playerOne);
  }

  /**
   * Sets the use of the Traditional Fleet for both players.
   */
  public void useTraditionalFleet() {
    playerOne.getFleet().setTraditional();
    playerTwo.getFleet().setTraditional();
  }

  /**
   * Sets the use of the Tactical Fleet for both players.
   */
  public void useTacticalFleet() {
    playerOne.getFleet().setTactical();
    playerTwo.getFleet().setTactical();
  }

  /**
   * Sets the use of the Custom Fleet fot both players.
   */
  public void useCustomFleet() {
    playerOne.getFleet().setCustom();
    playerTwo.getFleet().setCustom();
  }

  /**
   * Adds a new ship to the fleet of both players.
   * 
   * @param shipName the name of the new ship.
   * @param shipLength the length of the new ship.
   */
  public void addShip(String shipName, int shipLength) {
    playerOne.addShip(shipName, shipLength);
    playerTwo.addShip(shipName, shipLength);

  }

  /**
   * @return The actual fleet size.
   */
  public int getFleetSize() {
    return playerOne.getFleetSize();
  }

  /**
   * @return An iterator of the ships in the actual fleet.
   */
  public Iterator<IShip> getFleetIterator() {
    return playerOne.getFleetIterator();
  }

  /**
   * Sets the deploy strategy called tight.
   */
  public void useTightDeployment() {
    playerOne.setDeployment(new TightDeployment());
    playerTwo.setDeployment(new TightDeployment());
  }


  /**
   * Sets the deploy strategy called Spacious.
   */
  public void useSpaciousDeployment() {
    playerOne.setDeployment(new SpaciousDeployment());
    playerTwo.setDeployment(new SpaciousDeployment());
  }

  /**
   * Places the first available ship (not deployed) of the PlayerOne's fleet. horizontally in the
   * entered coordinates, from left to right.
   * 
   * @param horizontalPosition The horizontal start position for the ship.
   * @param verticalPosition The vertical start position for the ship
   * @return The deployed Ship if it was deployed, or a nullShip if the ship couldn't be deployed.
   */
  public IShip playerOneDeployHorizontally(int horizontalPosition, int verticalPosition) {
    return playerOne.deployHorizontally(horizontalPosition, verticalPosition);
  }

  /**
   * Places the first available ship (not deployed) of the PlayerOne's fleet. vertically in the
   * entered coordinates, from left to right.
   * 
   * @param horizontalPosition The horizontal start position for the ship.
   * @param verticalPosition The vertical start position for the ship
   * @return The deployed Ship if it was deployed, and a nullShip if the ship couldn't be deployed.
   */
  public IShip playerOneDeployVertically(int horizontalPosition, int verticalPosition) {
    return playerOne.deployVertically(horizontalPosition, verticalPosition);
  }


  /**
   * Places the first available ship (not deployed) of the PlayerTwo's fleet. horizontally in the
   * entered coordinates, from left to right.
   * 
   * @param horizontalPosition The horizontal start position for the ship.
   * @param verticalPosition The vertical start position for the ship
   * @return The deployed Ship if it was deployed, or a nullShip if the ship couldn't be deployed.
   */
  public IShip playerTwoDeployHorizontally(int horizontalPosition, int verticalPosition) {
    return playerTwo.deployHorizontally(horizontalPosition, verticalPosition);
  }

  /**
   * Places the first available ship (not deployed) of the PlayerTwo's fleet. vertically in the
   * entered coordinates, from left to right.
   * 
   * @param horizontalPosition The horizontal start position for the ship.
   * @param verticalPosition The vertical start position for the ship
   * @return The deployed Ship if it was deployed, and a nullShip if the ship couldn't be deployed.
   */
  public IShip playerTwoDeployVertically(int horizontalPosition, int verticalPosition) {
    return playerTwo.deployVertically(horizontalPosition, verticalPosition);
  }

  /**
   * @return The number of deployed ships of the PlayerOne.
   */
  public long playerOneNumberOfDeployedShips() {
    return playerOne.numberOfDeployedShips();
  }

  /**
   * @return The number of deployed ships of the PlayerOne.
   */
  public long playerOneNumberOfAnchoredShips() {
    return playerOne.numberOfAnchoredShips();
  }

  /**
   * @return The number of deployed ships of the PlayerTwo.
   */
  public long playerTwoNumberOfDeployedShips() {
    return playerTwo.numberOfDeployedShips();
  }

  /**
   * @return The number of deployed ships of the PlayerTwo.
   */
  public long playerTwoNumberOfAnchoredShips() {
    return playerTwo.numberOfAnchoredShips();
  }

  /**
   * @param horizontalPosition X coordinate to search.
   * @param verticalPosition Y coordinate to search.
   * @return true if there is a ship on the entered coordinates of the playerOne territory and false
   *         if not.
   */
  public boolean isPlayerOneShipAt(int horizontalPosition, int verticalPosition) {
    return playerOne.isShipAt(horizontalPosition, verticalPosition);
  }


  /**
   * @param horizontalPosition X coordinate to search.
   * @param verticalPosition Y coordinate to search.
   * @return true if there is a ship on the entered coordinates of the playerTwo territory and false
   *         if not.
   */
  public boolean isPlayerTwoShipAt(int horizontalPosition, int verticalPosition) {
    return playerTwo.isShipAt(horizontalPosition, verticalPosition);
  }

  /**
   * Returns the result of the attack of the player One.
   * 
   * @param horizontally X coordinate to attack.
   * @param vertically Y coordinate to attack.
   * @return An instance of {@link SuccessfulAttack}if it was a successful attack, and an instance
   *         of {@link UnsuccessfulAttack} if it was an unsuccessful attack.
   */
  public IAttackResult playerOneAttackAt(int horizontally, int vertically) {
    return playerTwo.attackedAt(horizontally, vertically) ? new SuccessfulAttack()
        : new UnsuccessfulAttack();
  }

  /**
   * Returns the result of the attack of the player Two.
   * 
   * @param horizontally X coordinate to attack.
   * @param vertically Y coordinate to attack.
   * @return An instance of {@link SuccessfulAttack}if it was a successful attack, and an instance
   *         of {@link UnsuccessfulAttack} if it was an unsuccessful attack.
   */
  public IAttackResult playerTwoAttackAt(int horizontally, int vertically) {
    return playerOne.attackedAt(horizontally, vertically) ? new SuccessfulAttack()
        : new UnsuccessfulAttack();
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + playerOne.hashCode();
    result = prime * result + playerTwo.hashCode();
    result = prime * result + desktopDisplay.hashCode();

    return result;
  }

  @Override
  public boolean equals(Object anObject) {
    return anObject instanceof Battleship
        && ((Battleship) anObject).playerOne.equals(this.playerOne)
        && ((Battleship) anObject).playerTwo.equals(this.playerTwo)
        && ((Battleship) anObject).desktopDisplay.equals(this.desktopDisplay) ? true : false;
  }
}
