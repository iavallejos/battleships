package battleship;

import battleship.ship.IShip;
import battleship.ship.NullShip;
import battleship.ship.Ship;

import java.util.Iterator;
import java.util.LinkedList;

/**
 * Fleet class consist of an LinkedList of objects of class IShip, it also has the number of
 * deployed ships It also contains 2 prefabricated fleets, the Tactical fleet and the Traditional
 * fleet, and a third empty fleet to be filled by the user.
 * 
 * @author Ignacio Vallejos.
 */
public class Fleet implements IFleet {
  private LinkedList<IShip> fleet;

  /**
   * Constructor for fleet, starts as a Traditional fleet.
   */
  public Fleet() {
    fleet = new LinkedList<IShip>();
    fleet.add(new Ship("Aircraft carrier", 5));
    fleet.add(new Ship("Battleship", 4));
    fleet.add(new Ship("Submarine", 3));
    fleet.add(new Ship("Destroyer", 3));
    fleet.add(new Ship("Patrol boat", 2));
  }

  /**
   * Sets the fleet as a Traditional fleet, which is composed by one Aircraft carrier of size 5, one
   * Battleship of size 4, one Submarine of size 3, one Destroyer of size 3 and one Patrol boat of
   * size 2.
   */
  public void setTraditional() {
    this.fleet = new LinkedList<IShip>();
    fleet.add(new Ship("Aircraft carrier", 5));
    fleet.add(new Ship("Battleship", 4));
    fleet.add(new Ship("Submarine", 3));
    fleet.add(new Ship("Destroyer", 3));
    fleet.add(new Ship("Patrol boat", 2));
  }

  /**
   * Sets the fleet as a Tactical fleet, which is composed by one Aircraft carrier of size 5, one
   * Battleship of size 4, one Cruiser of size 3, one Destroyer of size 2 and one Submarine of size
   * 1.
   */
  public void setTactical() {
    this.fleet = new LinkedList<IShip>();
    fleet.add(new Ship("Aircraft carrier", 5));
    fleet.add(new Ship("Battleship", 4));
    fleet.add(new Ship("Cruiser", 3));
    fleet.add(new Ship("Destroyer", 2));
    fleet.add(new Ship("Destroyer", 2));
    fleet.add(new Ship("Submarine", 1));
    fleet.add(new Ship("Submarine", 1));
  }

  /**
   * Sets the fleet as a Custom fleet, which is a empty fleet.
   */
  public void setCustom() {
    fleet = new LinkedList<IShip>();
  }

  @Override
  public void addShip(String name, int lenght) {
    fleet.add(new Ship(name, lenght));
  }

  @Override
  public Iterator<IShip> iterator() {
    return fleet.iterator();
  }

  @Override
  public int getSize() {
    return fleet.size();
  }

  @Override
  public IShip getAnchoredShip() {
    for (IShip ship : fleet) {
      if (ship.isAnchored()) {
        return ship;
      }
    }
    return new NullShip();
  }

  @Override
  public long numberOfAnchoredShips() {
    int anchored = 0;
    for (IShip ship : fleet) {
      if (ship.isAnchored()) {
        anchored++;
      }
    }
    return anchored;
  }

  @Override
  public long numberOfDeployedShips() {
    return this.getSize() - this.numberOfAnchoredShips();
  }

  @Override
  public LinkedList<IShip> getFleet() {
    return this.fleet;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + fleet.hashCode();
    return result;
  }

  @Override
  public boolean equals(Object anObject) {
    return anObject instanceof Fleet && ((Fleet) anObject).getFleet().equals(this.getFleet());
  }
}
