package battleship;

import battleship.ship.IShip;

/**
 * Deploys a ship horizontally in the territory.
 * 
 * @author Ignacio Vallejos.
 *
 */
public class HorizontalPosition implements IPosition {

  @Override
  public boolean deploy(int coordinateX, int coordinateY, IShip ship, Territory territory,
      IDeployment deployment) {
    if (deployment.deployable(coordinateX, coordinateY, coordinateX + ship.getLength() - 1,
        coordinateY, territory)) {
      territory.deployShip(coordinateX, coordinateY, coordinateX + ship.getLength() - 1,
          coordinateY, ship);
      return true;
    }
    return false;
  }

  @Override
  public int hashCode() {
    return 1;
  }

  @Override
  public boolean equals(Object anObject) {
    return anObject instanceof HorizontalPosition;
  }
}
