package battleship;

/**
 * Implementing this interface allows an user to check the deploy rules of the object.
 * 
 * @author Ignacio Vallejos.
 */
public interface IDeployment {

  /**
   * @param x0 initial x coordinate for the ship.
   * @param y0 initial y coordinate for the ship.
   * @param xf final x coordinate for the ship.
   * @param yf final y coordinate for the ship.
   * @param board The in to check.
   * @return True if the ship can be deployed and False if it can't.
   */
  boolean deployable(int x0, int y0, int xf, int yf, Territory board);


}
