package battleship;

import battleship.ship.IShip;

import java.util.Iterator;
import java.util.LinkedList;

/**
 * Implementing this interface allows an user to create an fleet of IShips.
 * 
 * @author Ignacio Vallejos.
 *
 */
public interface IFleet {
  /**
   * @return The iterator associated to the LinkedList of ships of the fleet.
   */
  public Iterator<IShip> iterator();

  /**
   * @return The amount of ships in the fleet.
   */
  public int getSize();

  /**
   * Search through the list looking for an anchored Ship , if it finds one it returns it, otherwise
   * returns a NullShip.
   * 
   * @return an undeployed Ship or an NullShip.
   */
  public IShip getAnchoredShip();

  /**
   * @return The number of anchored ships in the fleet.
   */
  public long numberOfAnchoredShips();

  /**
   * @return returns the number of deployed ships in the fleet.
   */
  public long numberOfDeployedShips();

  /**
   * Adds a new ship to the fleet.
   * 
   * @param name The name of the ship to be added.
   * @param length The length of the ship to be added.
   */
  public void addShip(String name, int length);

  /**
   * @return The LinkedList of IShips that represents the fleet.
   */
  public LinkedList<IShip> getFleet();
}
