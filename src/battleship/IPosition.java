package battleship;

import battleship.ship.IShip;

/**
 * Implementing this interface allows the user to deploy a ship in a territory.
 * 
 * @author Ignacio Vallejos.
 *
 */
public interface IPosition {
  /**
   * Tries do deploy the ship.
   * 
   * @param coordinateX The initial x coordinate for the ship.
   * @param coordinateY The initial y coordinate for the ship.
   * @param ship The ship to be deployed.
   * @param territory The territory where the ship will be deployed.
   * @param deployment The deployment rules for the deployment.
   * @return If can be deployed, it deploys it and returns true, if not it returns false.
   */
  boolean deploy(int coordinateX, int coordinateY, IShip ship, Territory territory,
      IDeployment deployment);
}
