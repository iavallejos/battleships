package battleship;

import battleship.display.IDisplay;
import battleship.display.TildeDisplay;
import battleship.ship.IShip;
import battleship.ship.NullShip;
import battleship.ship.Ship;

import java.util.Iterator;

/**
 * Player class has all the information of a player for the game battleship, it uses Territory,
 * Fleet, IDisplay, IDeploiment and IPosition for its methods.
 * 
 * @author Ignacio Vallejos.
 *
 */
public class Player {
  String name;
  Territory board;
  Fleet fleet;
  IDisplay display;
  IDeployment deployment;
  IPosition deployer;

  /**
   * Constructor for the class {@link Player} it sets its name to "a player", the display to
   * {@link TildeDisplay}, the {@link Deployment} to {@link SpaciousDeployment} and the deployer to
   * {@link HorizontalPosition}.
   */
  public Player() {
    name = "a player";
    board = new Territory();
    fleet = new Fleet();
    display = new TildeDisplay();
    deployment = new SpaciousDeployment();
    deployer = new HorizontalPosition();
  }

  /**
   * Adjusts the size of the territory.
   * 
   * @param width the desired width of the territory.
   * @param height the desired height of the territory.
   */
  public void setTerritorySize(int width, int height) {
    board.reSize(width, height);
  }

  /**
   * Sets the player's Name.
   * 
   * @param name The name to set.
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return The player's name.
   */
  public String getName() {
    return this.name;
  }

  /**
   * @return The territory of the player.
   */
  public Territory getTerritory() {
    return board;
  }

  /**
   * @return The player's Display.
   */
  public IDisplay getDisplay() {
    return display;
  }

  /**
   * @return The height of the territory.
   */
  public int getTerritoryHeight() {
    return board.getHeight();
  }

  /**
   * @return The width of the territory.
   */
  public int getTerritoryWidth() {
    return board.getWidth();
  }


  /**
   * @param display The display to be set.
   */
  public void setDisplay(IDisplay display) {
    this.display = display;
  }

  /**
   * @return A string that represents the territory drawn according to the established display.
   */
  public String displayTerritory() {
    return display.display(board);
  }

  /**
   * @return The fleet of the player.
   */
  public Fleet getFleet() {
    return this.fleet;
  }

  /**
   * @param deployment The deployment to be set.
   */
  public void setDeployment(IDeployment deployment) {
    this.deployment = deployment;
  }

  /**
   * Adds a new ship to the fleet.
   * 
   * @param shipName the name of the new ship.
   * @param shipLength the length of the new ship.
   */
  public void addShip(String shipName, int shipLength) {
    fleet.addShip(shipName, shipLength);
  }

  /**
   * @return The actual fleet size.
   */
  public int getFleetSize() {
    return fleet.getSize();
  }

  /**
   * @return An iterator of the ships in the actual fleet.
   */
  public Iterator<IShip> getFleetIterator() {
    return fleet.iterator();
  }

  /**
   * Places the first available ship (not deployed) of the actual fleet. horizontally in the entered
   * coordinates, from left to right.
   * 
   * @param horizontalPosition The horizontal start position for the ship.
   * @param verticalPosition The vertical start position for the ship
   * @return The deployed Ship if it was deployed, or a nullShip if the ship couldn't be deployed.
   */
  public IShip deployHorizontally(int horizontalPosition, int verticalPosition) {
    deployer = new HorizontalPosition();
    IShip deployed = fleet.getAnchoredShip();
    if (deployed.equals(new NullShip())) {
      return deployed;
    } else if (deployer.deploy(horizontalPosition, verticalPosition, deployed, board, deployment)) {
      return deployed;
    }
    return new NullShip();
  }

  /**
   * Places the first available ship (not deployed) of the actual fleet. vertically in the entered
   * coordinates, from left to right.
   * 
   * @param horizontalPosition The horizontal start position for the ship.
   * @param verticalPosition The vertical start position for the ship
   * @return The deployed Ship if it was deployed, and a nullShip if the ship couldn't be deployed.
   */
  public IShip deployVertically(int horizontalPosition, int verticalPosition) {
    deployer = new VerticalPosition();
    IShip deployed = fleet.getAnchoredShip();
    if (deployed.equals(new NullShip())) {
      return deployed;
    } else if (deployer.deploy(horizontalPosition, verticalPosition, deployed, board, deployment)) {
      return deployed;
    }
    return new NullShip();
  }

  /**
   * @return The number of deployed ships of the actual fleet.
   */
  public long numberOfDeployedShips() {
    return fleet.numberOfDeployedShips();
  }

  /**
   * @return The number of anchored ships of the actual fleet.
   */
  public long numberOfAnchoredShips() {
    return fleet.numberOfAnchoredShips();
  }

  /**
   * @param horizontalPosition X coordinate to search.
   * @param verticalPosition Y coordinate to search.
   * @return true if there is a ship on the entered coordinates and false if not.
   */
  public boolean isShipAt(int horizontalPosition, int verticalPosition) {
    return board.shipAt(horizontalPosition, verticalPosition);
  }

  /**
   * Attacks the player in the entered coordinates.
   * 
   * @param horizontalPosition X coordinate to attack.
   * @param verticalPosition Y coordinate to attack.
   * @return true if in the coordinate is a {@link Ship}, false if not.
   */
  public boolean attackedAt(int horizontalPosition, int verticalPosition) {
    return board.attack(horizontalPosition, verticalPosition);
  }


  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + name.hashCode();
    result = prime * result + board.hashCode();
    result = prime * result + fleet.hashCode();
    result = prime * result + display.hashCode();
    result = prime * result + deployment.hashCode();
    return result;
  }

  @Override
  public boolean equals(Object anObject) {
    return anObject instanceof Player && ((Player) anObject).fleet.equals(this.fleet)
        && ((Player) anObject).name.equals(this.name)
        && ((Player) anObject).board.equals(this.board)
        && ((Player) anObject).display.equals(this.display)
        && ((Player) anObject).deployment.equals(this.deployment) ? true : false;
  }
}
