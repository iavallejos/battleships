package battleship;

/**
 * Checks the Spacious Deployment rules.
 * 
 * @author Ignacio Vallejos
 *
 */
public class SpaciousDeployment extends AbstractDeployment {

  @Override
  boolean deploymentRule(int x0, int y0, int xf, int yf, Territory territory) {
    int leftBorder = underTruncate(1, x0 - 1);
    int rightBorder = upperTruncate(territory.getWidth() , xf + 1);
    int topBorder = underTruncate(1, y0 - 1);
    int bottomBorder = upperTruncate(territory.getHeight(), yf + 1);

    for (int i = leftBorder; i < rightBorder + 1; i++) {
      for (int j = topBorder; j < bottomBorder + 1; j++) {
        if (territory.shipAt(i, j)) {
          return false;
        }
      }
    }
    return true;
  }

  /**
   * Auxiliary method used by deploymentRule to not look outside the grid.
   * 
   * @param min The minimum size of the grid.
   * @param number The number to analyze.
   * @return If the number is smallest than the minimum it returns the minimum, if not, it returns
   *         the number.
   */
  private int underTruncate(int min, int number) {
    return number < min ? min : number;
  }

  /**
   * Auxiliary method used by deploymentRule to not look outside the grid.
   * 
   * @param max The maximum size of the grid.
   * @param number The number to analyze.
   * @return If the number is bigger than the maximum it returns the maximum, if not, it returns the
   *         number.
   */
  private int upperTruncate(int max, int number) {
    return number > max ? max : number;
  }

  @Override
  public int hashCode() {
    return 1;
  }

  @Override
  public boolean equals(Object anObject) {
    return anObject instanceof SpaciousDeployment;
  }
}
