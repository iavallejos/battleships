package battleship;

import battleship.ship.IShip;
import battleship.ship.Ship;
import battleship.spot.ISpot;
import battleship.spot.ShipState;
import battleship.spot.SinkedShip;
import battleship.spot.Spot;

import java.util.Arrays;

/**
 * The Territory class represents a grid for the game BattleShips, in this grid the ships can be
 * represented, it has 2 methods of visualization, the Tilde visualization and the Space
 * visualization, it also has 2 methods of strategic deployment, the Spacious deployment and the
 * Tight deployment.
 * 
 * @author Ignacio Vallejos.
 */
public class Territory {
  private int width;
  private int height;
  private ISpot[][] battlefield;

  /**
   * Constructor for the class Territory, creates an square grid of. 10x10 and sets the display to
   * Tilde and the deployment to Spacious.
   */
  public Territory() {
    reSize(10, 10);
  }

  /**
   * getWidth is a getter for the width of the grid.
   * 
   * @return width of the territory.
   */
  public int getWidth() {
    return width;
  }

  /**
   * getHeight is a getter for the height of the grid.
   * 
   * @return height of the territory.
   */
  public int getHeight() {
    return height;
  }

  /**
   * Changes the size of the grid setting all squares of the grid to a water Spot.
   * 
   * @param width The new width of the territory must be an integer greater than 0.
   * @param height The new height of the territory, must be an integer greater than 0.
   */
  public void reSize(int width, int height) {
    this.width = width;
    this.height = height;
    this.battlefield = new ISpot[this.width][this.height];
    for (int i = 0; i < this.battlefield.length; i++) { // X axis.
      for (int j = 0; j < this.battlefield[i].length; j++) { // Y axis.
        battlefield[i][j] = new Spot();
      }
    }
  }

  /**
   * @return The territory battlefield.
   */
  public ISpot[][] getBattlefield() {
    return this.battlefield;
  }

  /**
   * Deploys a Ship between the specified squares of the grid.
   * 
   * @param x0 The initial x coordinate for the ship.
   * @param y0 The initial y coordinate for the ship.
   * @param xf The final x coordinate for the ship.
   * @param yf The final y coordinate for the ship.
   * @param ship The ship to be deployed.
   */
  public void deployShip(int x0, int y0, int xf, int yf, IShip ship) {
    for (int i = x0; i <= xf; i++) {
      for (int j = y0; j <= yf; j++) {
        battlefield[j - 1][i - 1].setState(new ShipState(ship));
      }
    }
    ship.deploy();
  }

  /**
   * @param coordinateX The x coordinate to be checked.
   * @param coordinateY The y coordinate to be checked.
   * @return true if there is a ship in that coordinate, false if not.
   */
  public boolean shipAt(int coordinateX, int coordinateY) {
    return battlefield[coordinateY - 1][coordinateX - 1].hasShip();
  }

  /**
   * Attacks the (x,y) square of the grid.
   * 
   * @param coordinateX The x coordinate to be attacked.
   * @param coordinateY The y coordinate to be attacked.
   * @return true if it "hit" a ship, false if not.
   */
  public boolean attack(int coordinateX, int coordinateY) {
    try {
      boolean result = battlefield[coordinateY - 1][coordinateX - 1].attack();
      IShip sinked = battlefield[coordinateY - 1][coordinateX - 1].getState().getShip();
      if (result && sinked.isSinked()) {
        for (int i = 0; i < width; i++) {
          for (int j = 0; j < height; j++) {
            if (sinked.equals(battlefield[j][i].getState().getShip())) {
              battlefield[j][i].setState(new SinkedShip(sinked));
            }
          }
        }
      }
      return result;
    } catch (Exception exception) {
      return false;
    }
  }

  /**
   * @return The number of {@link Ship} parts in the territory.
   */
  public int totalShipParts() {
    int count = 0;
    for (ISpot[] row : battlefield) {
      for (ISpot spot : row) {
        if (spot.hasShip()) {
          count++;
        }
      }
    }
    return count;
  }

  /**
   * @return The number of attacked {@link Ship} parts in the territory.
   */
  public int affectedShipParts() {
    int count = 0;
    for (ISpot[] row : battlefield) {
      for (ISpot spot : row) {
        if (spot.hasAttackedShip()) {
          count++;
        }
      }
    }
    return count;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + Arrays.deepHashCode(battlefield);
    result = prime * result + height;
    result = prime * result + width;
    return result;
  }

  @Override
  public boolean equals(Object anObject) {

    return anObject instanceof Territory && ((Territory) anObject).getHeight() == this.getHeight()
        && ((Territory) anObject).getWidth() == this.getWidth();
  }
}
