package battleship;

/**
 * Checks the Tight Deployment rules.
 * 
 * @author Ignacio Vallejos.
 *
 */
public class TightDeployment extends AbstractDeployment {

  @Override
  boolean deploymentRule(int x0, int y0, int xf, int yf, Territory territory) {
    int leftBorder = x0;
    int rightBorder = xf;
    int topBorder = y0;
    int bottomBorder = yf;

    for (int i = leftBorder; i < rightBorder + 1; i++) {
      for (int j = topBorder; j < bottomBorder + 1; j++) {
        if (territory.shipAt(i, j)) {
          return false;
        }
      }
    }
    return true;
  }

  @Override
  public int hashCode() {
    return 1;
  }

  @Override
  public boolean equals(Object anObject) {
    return anObject instanceof TightDeployment;
  }
}
