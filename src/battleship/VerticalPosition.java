package battleship;

import battleship.ship.IShip;

/**
 * Deploys a ship vertically on the territory.
 * 
 * @author Ignacio Vallejos.
 *
 */
public class VerticalPosition implements IPosition {

  @Override
  public boolean deploy(int coordinateX, int coordinateY, IShip ship, Territory territory,
      IDeployment deployment) {
    if (deployment.deployable(coordinateX, coordinateY, coordinateX,
        coordinateY + ship.getLength() - 1, territory)) {
      territory.deployShip(coordinateX, coordinateY, coordinateX,
          coordinateY + ship.getLength() - 1, ship);
      return true;
    }
    return false;
  }

  @Override
  public int hashCode() {
    return 1;
  }

  @Override
  public boolean equals(Object anObject) {
    return anObject instanceof VerticalPosition;
  }
}
