package battleship.attack;

/**
 * Implementing this interface allows an user to know if the attack was successful or not.
 * 
 * @author Ignacio Vallejos.
 *
 */
public interface IAttackResult {
  /**
   * @return true if the attack was successful, false if not.
   */
  boolean isAffectedTarget();

  /**
   * @return true if the attack missed, false if not.
   */
  boolean isMissedTarget();
}
