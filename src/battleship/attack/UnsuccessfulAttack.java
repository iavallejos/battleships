package battleship.attack;

/**
 * The NullShip class implements the interface IAttackResult and its used as a variable to know if
 * the attack was successful or not.
 * 
 * @author Ignacio Vallejos.
 */
public class UnsuccessfulAttack implements IAttackResult {

  @Override
  public boolean isAffectedTarget() {
    return false;
  }

  @Override
  public boolean isMissedTarget() {
    return true;
  }

}
