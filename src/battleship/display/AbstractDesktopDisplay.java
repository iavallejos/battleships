package battleship.display;

import battleship.Player;

/**
 * The DesktopDisplay Class uses an StringBuilder to create the desktop String.
 * 
 * @author Ignacio Vallejos.
 *
 */
public abstract class AbstractDesktopDisplay implements IDesktopDisplay {
  private StringBuilder stream;

  /**
   * Constructor for the class, it initializes the StringBuilder.
   */
  public AbstractDesktopDisplay() {
    stream = new StringBuilder();
  }

  @Override
  public String displayDesktop(Player player1, Player player2) {
    stream = new StringBuilder();
    stream.append("Player: " + player1.getName());
    stream.append(System.lineSeparator());

    stream.append(" with " + player1.getTerritory().affectedShipParts() + " out of "
        + player1.getTerritory().totalShipParts() + " affected ship parts.");
    stream.append(System.lineSeparator());
    stream.append(System.lineSeparator());

    stream.append(displayDesktop(player1, player1.getDisplay()));
    stream.append(System.lineSeparator());

    stream.append("Opponent player: " + player2.getName());
    stream.append(System.lineSeparator());

    stream.append(" with " + player2.getTerritory().affectedShipParts() + " out of "
        + player2.getTerritory().totalShipParts() + " affected ship parts.");
    stream.append(System.lineSeparator());
    stream.append(System.lineSeparator());

    stream.append(displayDesktop(player2, new OponnentDisplay()));

    return stream.toString();
  }

  /**
   * It creates an String that represents a territory for the desktopDisplay using a given display.
   * 
   * @param player The player whose territory is getting "drawn".
   * @param display The IDisplay to use for drawing.
   * @return A string that represents the desktop of the player.
   */
  abstract String displayDesktop(Player player, IDisplay display);

}
