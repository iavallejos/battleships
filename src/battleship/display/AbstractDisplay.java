package battleship.display;

import battleship.Territory;
import battleship.spot.AttackedShip;
import battleship.spot.AttackedWater;
import battleship.spot.ISpot;
import battleship.spot.ShipState;
import battleship.spot.SinkedShip;
import battleship.spot.Water;

/**
 * The Display Class uses an StringBuilder to create the board String.
 * 
 * @author Ignacio Vallejos.
 *
 */
public abstract class AbstractDisplay implements IDisplay {
  private StringBuilder stream;

  /**
   * Constructor for the class, it initializes the StringBuilder.
   */
  public AbstractDisplay() {
    stream = new StringBuilder();
  }

  @Override
  public String display(Territory territory) {
    String board = "";
    for (ISpot[] column : territory.getBattlefield()) {
      for (ISpot spot : column) {
        stream.append(spot.display(this));
      }
      stream.append(System.lineSeparator());
    }
    board = stream.toString();
    stream = new StringBuilder();
    return board;
  }

  @Override
  public abstract String waterSymbol(Water spot);

  @Override
  public String attackedWaterSymbol(AttackedWater spot) {
    return ".";
  }

  @Override
  public abstract String shipSymbol(ShipState spot);

  @Override
  public abstract String attackedShipSymbol(AttackedShip spot);

  @Override
  public String sinkedShipSymbol(SinkedShip spot) {
    return "#";
  }
}
