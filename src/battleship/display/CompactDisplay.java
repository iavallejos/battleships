package battleship.display;

import battleship.Player;

/**
 * The CompactDisplay implements the IDesktopDisplay interface and its used to draw the desktop
 * using the Compact desktop display mode.
 * 
 * @author Ignacio Vallejos.
 *
 */
public class CompactDisplay extends AbstractDesktopDisplay {

  @Override
  String displayDesktop(Player player, IDisplay display) {
    return display.display(player.getTerritory());
  }

  @Override
  public int hashCode() {
    return 1;
  }

  @Override
  public boolean equals(Object anObject) {
    return anObject instanceof CompactDisplay;
  }
}
