package battleship.display;

import battleship.Player;
import battleship.spot.ISpot;

/**
 * The DesktopDisplay implements the IDesktopDisplay interface and its used to draw the desktop
 * using the classic desktop display mode.
 * 
 * @author Ignacio Vallejos.
 *
 */
public class DesktopDisplay extends AbstractDesktopDisplay {

  @Override
  String displayDesktop(Player player, IDisplay display) {
    int startChar = 65;
    int cont = 1;
    StringBuilder desktopOneBuilder = new StringBuilder();
    desktopOneBuilder.append("   ");
    for (int i = 0; i < player.getTerritoryWidth(); i++) {
      desktopOneBuilder.append((char) startChar);
      startChar++;
    }
    desktopOneBuilder.append(System.lineSeparator());
    for (ISpot[] column : player.getTerritory().getBattlefield()) {
      if (cont < 10) {
        desktopOneBuilder.append(" ");
      }
      desktopOneBuilder.append(cont + " ");
      for (ISpot spot : column) {
        desktopOneBuilder.append(spot.display(display));
      }
      desktopOneBuilder.append(System.lineSeparator());
      cont++;
    }
    return desktopOneBuilder.toString();
  }

  @Override
  public int hashCode() {
    return 1;
  }

  @Override
  public boolean equals(Object anObject) {
    return anObject instanceof DesktopDisplay;
  }
}
