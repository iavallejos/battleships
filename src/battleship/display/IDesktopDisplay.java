package battleship.display;

import battleship.Player;

/**
 * Implementing this interface allows an user to draw the desktop of a player.
 * 
 * @author Ignacio Vallejos.
 *
 */
public interface IDesktopDisplay {
  /**
   * @param player1 The player who is playing.
   * @param player2 The opponent player.
   * @return A string that represents the desktop of a player.
   */
  String displayDesktop(Player player1, Player player2);
}
