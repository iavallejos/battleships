package battleship.display;

import battleship.Territory;
import battleship.spot.AttackedShip;
import battleship.spot.AttackedWater;
import battleship.spot.ShipState;
import battleship.spot.SinkedShip;
import battleship.spot.Water;

/**
 * Implementing this interface allows an user to draw the board.
 * 
 * @author Ignacio Vallejos.
 *
 */
public interface IDisplay {
  /**
   * @return The String that represents the board using the selected Display mode.
   */
  public String display(Territory territory);

  /**
   * @param spot The spot from where the symbol is obtained.
   * @return The water symbol of the corresponding Display mode.
   */
  public abstract String waterSymbol(Water spot);

  /**
   * @param spot The spot from where the symbol is obtained.
   * @return The attacked water symbol of the corresponding Display mode.
   */
  public String attackedWaterSymbol(AttackedWater spot);

  /**
   * @param spot The spot from where the symbol is obtained.
   * @return The ship symbol of the corresponding Display mode.
   */
  public abstract String shipSymbol(ShipState spot);

  /**
   * @param spot The spot from where the symbol is obtained.
   * @return The attacked ship symbol of the corresponding Display mode.
   */
  public abstract String attackedShipSymbol(AttackedShip spot);

  /**
   * @param sinkedShip The spot from where the symbol is obtained.
   * @return The sinked ship symbol of the corresponding Display mode.
   */
  public String sinkedShipSymbol(SinkedShip sinkedShip);
}
