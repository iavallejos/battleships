package battleship.display;

import battleship.spot.AttackedShip;
import battleship.spot.AttackedWater;
import battleship.spot.ShipState;
import battleship.spot.Water;

/**
 * The OponnentDisplay implements the IDisplay interface and its used to draw the board using the
 * Opponent display mode, its used to draw the oponent's board in the desktop display.
 * 
 * @author Ignacio Vallejos.
 *
 */
public class OponnentDisplay extends AbstractDisplay {

  @Override
  public String waterSymbol(Water spot) {
    return " ";
  }

  @Override
  public String attackedWaterSymbol(AttackedWater spot) {
    return "/";
  }

  @Override
  public String shipSymbol(ShipState spot) {
    return " ";
  }

  @Override
  public String attackedShipSymbol(AttackedShip spot) {
    return "x";
  }

  @Override
  public int hashCode() {
    return 1;
  }

  @Override
  public boolean equals(Object anObject) {
    return anObject instanceof OponnentDisplay;
  }
}
