package battleship.display;

import battleship.Player;
import battleship.spot.ISpot;

/**
 * The ReverseDesktopDisplay implements the IDesktopDisplay interface and its used to draw the
 * desktop using the reverse desktop display mode.
 * 
 * @author Ignacio Vallejos.
 *
 */
public class ReverseDesktopDisplay extends AbstractDesktopDisplay {

  @Override
  String displayDesktop(Player player, IDisplay display) {
    int startChar = 65;
    String top1 = "  ";
    String top2 = "  ";
    StringBuilder desktopOneBuilder = new StringBuilder();


    for (int i = 1; i < player.getTerritoryWidth() + 1; i++) {
      if (i < 10) {
        top1 += " ";
      } else {
        top1 += (i - i % 10) / 10;
      }
      top2 += i % 10;
    }
    if (player.getTerritoryWidth() > 9) {
      desktopOneBuilder.append(top1);
      desktopOneBuilder.append(System.lineSeparator());
    }
    desktopOneBuilder.append(top2);
    desktopOneBuilder.append(System.lineSeparator());
    for (ISpot[] column : player.getTerritory().getBattlefield()) {
      desktopOneBuilder.append((char) startChar + " ");
      for (ISpot spot : column) {
        desktopOneBuilder.append(spot.display(display));
      }
      desktopOneBuilder.append(System.lineSeparator());
      startChar++;
    }
    return desktopOneBuilder.toString();
  }

  @Override
  public int hashCode() {
    return 1;
  }

  @Override
  public boolean equals(Object anObject) {
    return anObject instanceof ReverseDesktopDisplay;
  }
}
