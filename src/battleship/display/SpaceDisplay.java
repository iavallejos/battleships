package battleship.display;

import battleship.spot.AttackedShip;
import battleship.spot.ShipState;
import battleship.spot.Water;

/**
 * The SpaceDisplay implements the IDisplay interface and its used to draw the board using the Space
 * display mode.
 * 
 * @author Ignacio Vallejos.
 *
 */
public class SpaceDisplay extends AbstractDisplay {

  @Override
  public String waterSymbol(Water spot) {
    return " ";
  }

  @Override
  public String shipSymbol(ShipState spot) {
    return "*";
  }

  @Override
  public String attackedShipSymbol(AttackedShip spot) {
    return "x";
  }

  @Override
  public int hashCode() {
    return 1;
  }

  @Override
  public boolean equals(Object anObject) {
    return anObject instanceof SpaceDisplay;
  }
}
