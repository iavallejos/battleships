package battleship.display;

import battleship.spot.AttackedShip;
import battleship.spot.ShipState;
import battleship.spot.Water;


/**
 * The TildeDisplay implements the IDisplay interface and its used to draw the board using the Tilde
 * display mode.
 * 
 * @author Ignacio Vallejos.
 *
 */
public class TildeDisplay extends AbstractDisplay {

  @Override
  public String waterSymbol(Water spot) {
    return "~";
  }

  @Override
  public String shipSymbol(ShipState spot) {
    return "o";
  }

  @Override
  public String attackedShipSymbol(AttackedShip spot) {
    return "+";
  }

  @Override
  public int hashCode() {
    return 1;
  }

  @Override
  public boolean equals(Object anObject) {
    return anObject instanceof TildeDisplay;
  }
}
