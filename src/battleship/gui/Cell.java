package battleship.gui;

import battleship.spot.State;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

import java.util.Observable;
import java.util.Observer;

/**
 * A cell that represents one square of the grid of the battleship game.
 * 
 * @author Ignacio Vallejos.
 *
 */
public class Cell extends Rectangle implements Observer {
  Color displayColor;
  int coordinateX;
  int coordinateY;

  /**
   * Creates a cell for the grid of the game.
   * 
   * @param coordinateX The x coordinate of the cell in the grid.
   * @param coordinateY The y coordinate of the cell in the grid.
   */
  public Cell(int coordinateX, int coordinateY) {
    super(30, 30);
    displayColor = Color.BLUE;
    this.paint(displayColor);
    this.coordinateX = coordinateX;
    this.coordinateY = coordinateY;
  }

  public Color getColor() {
    return this.displayColor;
  }

  public int getCoordinateX() {
    return this.coordinateX;
  }

  public int getCoordinateY() {
    return this.coordinateY;
  }

  public void paint(Color color) {
    this.setFill(color);
    this.setStroke(Color.BLACK);
  }

  @Override
  public void update(Observable obj, Object arg) {
    displayColor = ((State) arg).getColor();
    this.paint(displayColor);
    this.setDisable(true);
  }
}
