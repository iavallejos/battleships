package battleship.gui;

import battleship.Player;
import battleship.Territory;
import battleship.spot.Spot;
import javafx.scene.Parent;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

/**
 * A board that represents the grid of the battleship game it's used to deploy ships.
 * 
 * @author Ignacio Vallejos.
 *
 */
public class DeploymentBoard extends Parent {
  private VBox rows = new VBox();

  /**
   * A grid that represents the board of a player, it's used to deploy ships.
   * 
   * @param player The player whose board will be represented.
   */
  public DeploymentBoard(Player player) {
    Territory territory = player.getTerritory();
    for (int y = 1; y < territory.getHeight() + 1; y++) {
      HBox row = new HBox();
      for (int x = 1; x < territory.getWidth() + 1; x++) {
        Cell spot = new Cell(x, y);
        ((Spot) territory.getBattlefield()[y - 1][x - 1]).addObserver(spot);

        spot.setOnMouseEntered(action -> {
          spot.paint(Color.RED);
          spot.setStroke(Color.AQUA);
        });
        spot.setOnMouseExited(action -> spot.paint(spot.getColor()));

        spot.setOnMousePressed(click -> {
          if (click.getButton() == MouseButton.PRIMARY) {
            player.deployVertically(spot.getCoordinateX(), spot.getCoordinateY());
          } else if (click.getButton() == MouseButton.SECONDARY) {
            player.deployHorizontally(spot.getCoordinateX(), spot.getCoordinateY());
          }

        });
        row.getChildren().add(spot);
      }
      rows.getChildren().add(row);
    }
    getChildren().add(rows);
  }
}
