package battleship.gui;

import battleship.Battleship;
import battleship.gui.scenes.LogInScene;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * The class that runs the game.
 * 
 * @author Ignacio Vallejos.
 *
 */
public class Main extends Application {
  Scene scene;
  Battleship game = new Battleship();

  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage primaryStage) throws Exception {
    primaryStage.setTitle("Battleship");
    scene = new Scene(LogInScene.display(game, primaryStage), 320, 260);
    primaryStage.setScene(scene);
    primaryStage.setResizable(false);
    primaryStage.show();
  }



}
