package battleship.gui;

import battleship.Player;
import battleship.Territory;
import battleship.gui.scenes.WinScene;
import battleship.ship.NullShip;
import battleship.spot.SinkedShip;
import battleship.spot.Spot;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.Observable;
import java.util.Observer;

/**
 * A board that represents the grid of the battleship game it's used to attack.
 * 
 * @author Ignacio Vallejos
 *
 */
public class PlayBoard extends Parent implements Observer {
  private VBox rows = new VBox();
  boolean playing;
  PlayBoard oponent;
  Player player;
  Text turnText;
  Stage window;

  /**
   * A grid that represents the board of a player, it's used to attack ships.
   * 
   * @param player The player whose board will be represented.
   * @param turn represents if is the player's turn at the moment of creation.
   * @param turnText a Text that represents whose turn is.
   * @param window The window where the game is running.
   */
  public PlayBoard(Player player, boolean turn, Text turnText, Stage window) {
    this.playing = turn;
    this.player = player;
    this.turnText = turnText;
    this.window = window;

    Territory territory = player.getTerritory();
    for (int y = 1; y < territory.getHeight() + 1; y++) {
      HBox row = new HBox();
      for (int x = 1; x < territory.getWidth() + 1; x++) {
        Cell spot = new Cell(x, y);
        ((Spot) territory.getBattlefield()[y - 1][x - 1]).addObserver(spot);
        ((Spot) territory.getBattlefield()[y - 1][x - 1]).addObserver(this);

        spot.setOnMouseEntered(action -> {
          spot.paint(Color.RED);
          spot.setStroke(Color.AQUA);
        });
        spot.setOnMouseExited(action -> spot.paint(spot.getColor()));

        spot.setOnMousePressed(
            click -> player.attackedAt(spot.getCoordinateX(), spot.getCoordinateY()));
        row.getChildren().add(spot);
      }
      rows.getChildren().add(row);
    }
    getChildren().add(rows);
    this.setDisable(playing);
  }

  public void setOponent(PlayBoard oponent) {
    this.oponent = oponent;
  }


  public void passTurn() {
    this.switchBoard();
    oponent.switchBoard();
  }

  public void switchBoard() {
    this.playing = !this.playing;
    this.setDisable(playing);
  }

  public boolean isPlaying() {
    return this.playing;
  }

  @Override
  public void update(Observable obs, Object arg) {
    if (arg.getClass() != new SinkedShip(new NullShip()).getClass()) {
      passTurn();
      turnText.setText(this.getName() + "\nis attacking");
      if (player.getTerritory().affectedShipParts() == player.getTerritory().totalShipParts()) {
        Scene winScene = new Scene(WinScene.display(oponent.getName(), window), 400, 300);
        window.setScene(winScene);
      }
    }
  }

  public String getName() {
    return player.getName();
  }
}
