package battleship.gui.scenes;

import battleship.Battleship;
import battleship.gui.DeploymentBoard;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * The scene of the battleship game where the players deploys their ships.
 * 
 * @author Ignacio Vallejos.
 *
 */
public class DeploymentScene {
  static boolean playerOneTurn;

  /**
   * Creates a DeploymentScene for the game.
   * 
   * @param game The game that is been used.
   * @param window The window where the game is running.
   * @return A BordedPane with the Scene.
   */
  public static BorderPane display(Battleship game, Stage window) {
    GridPane playScreen = new GridPane();
    GridPane resetBox = new GridPane();
    playScreen.setPadding(new Insets(0, 15, 0, 15));
    resetBox.setPadding(new Insets(0, 10, 10, 130));
    final Text titleText = new Text("BATTLESHIP");
    titleText.setStyle("-fx-font: 48 Algerian;");

    BorderPane screen = new BorderPane();
    HBox titleBox = new HBox();
    titleBox.getChildren().add(titleText);
    titleBox.setAlignment(Pos.CENTER);
    screen.setTop(titleBox);

    playerOneTurn = true;
    Text playerOneName = new Text(game.getPlayerOneName() + " is deploying his ships");
    playerOneName.setStyle("-fx-font: 20 arial");
    DeploymentBoard playerOneDeploymentBoard = new DeploymentBoard(game.playerOne);

    VBox gameBox = new VBox();
    gameBox.getChildren().addAll(playerOneName, playerOneDeploymentBoard);
    GridPane.setConstraints(gameBox, 1, 1);
    playScreen.getChildren().add(gameBox);

    screen.setCenter(playScreen);

    screen.setOnMouseReleased(click -> {
      if (game.playerOneNumberOfAnchoredShips() == 0 && playerOneTurn) {
        Text playerTwoName = new Text(game.getPlayerTwoName() + " is deploying his ships");
        playerTwoName.setStyle("-fx-font: 20 arial");
        DeploymentBoard playerTwoDeploymentBoard = new DeploymentBoard(game.playerTwo);
        gameBox.getChildren().removeAll(playerOneName, playerOneDeploymentBoard);
        gameBox.getChildren().addAll(playerTwoName, playerTwoDeploymentBoard);
        playerOneTurn = false;
      } else if (game.playerTwoNumberOfAnchoredShips() == 0) {
        Scene deploymentScene = new Scene(PlayScene.display(game, window), 780, 450);
        window.setScene(deploymentScene);
      }
    });

    Button restartButton = new Button("Restart");
    restartButton.setMinWidth(80);
    restartButton.setOnAction(action -> {
      Scene scene = new Scene(LogInScene.display(new Battleship(), window), 320, 270);
      window.setScene(scene);
    });
    resetBox.getChildren().add(restartButton);
    screen.setBottom(resetBox);
    return screen;

  }
}
