package battleship.gui.scenes;


import battleship.Battleship;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * The scene of the battleship game where the players writes their names and set the deployment
 * rules and the fleet to use.
 * 
 * @author Ignacio Vallejos.
 *
 */
public class LogInScene {

  /**
   * Creates a LogInScene for the game.
   * 
   * @param game The game that is been used.
   * @param window The window where the game is running.
   * @return A BordedPane with the Scene.
   */
  public static BorderPane display(Battleship game, Stage window) {
    GridPane selectionScreen = new GridPane();
    GridPane playScreen = new GridPane();

    final Text titleText = new Text("BATTLESHIP");
    titleText.setStyle("-fx-font: 48 Algerian;");

    playScreen.setPadding(new Insets(0, 10, 10, 135));

    selectionScreen.setPadding(new Insets(0, 10, 0, 10));
    selectionScreen.setVgap(10);
    selectionScreen.setHgap(10);

    GridPane nameScreen = new GridPane();
    nameScreen.setPadding(new Insets(0, 10, 0, 10));
    nameScreen.setVgap(10);
    nameScreen.setHgap(10);

    final Text playerNamesText = new Text("Enter player names:");
    playerNamesText.setStyle("-fx-font: 20 arial");
    GridPane.setConstraints(playerNamesText, 1, 1);

    TextField playerOneNameTextField = new TextField();
    playerOneNameTextField.setPromptText("Player One Name");
    playerOneNameTextField.setMinWidth(280);

    GridPane.setConstraints(playerOneNameTextField, 1, 2);

    TextField playerTwoNameTextField = new TextField();
    playerTwoNameTextField.setPromptText("Player Two Name");
    GridPane.setConstraints(playerTwoNameTextField, 1, 3);

    nameScreen.getChildren().addAll(playerNamesText, playerOneNameTextField,
        playerTwoNameTextField);


    final ToggleGroup fleetSelection = new ToggleGroup();
    final ToggleGroup deploymentSelection = new ToggleGroup();

    RadioButton tacticalFleet = new RadioButton("Tactical Fleet");
    tacticalFleet.setOnAction(action -> game.useTacticalFleet());
    RadioButton traditionalFleet = new RadioButton("Traditional Fleet");
    traditionalFleet.setOnAction(action -> game.useTraditionalFleet());

    tacticalFleet.setToggleGroup(fleetSelection);
    traditionalFleet.setToggleGroup(fleetSelection);
    traditionalFleet.setSelected(true);

    RadioButton spaciousDeployment = new RadioButton("Spacious Deployment");
    spaciousDeployment.setOnAction(action -> game.useSpaciousDeployment());
    RadioButton tightDeployment = new RadioButton("Tight Deployment");
    tightDeployment.setOnAction(action -> game.useTightDeployment());

    tightDeployment.setToggleGroup(deploymentSelection);
    spaciousDeployment.setToggleGroup(deploymentSelection);
    spaciousDeployment.setSelected(true);

    GridPane.setConstraints(spaciousDeployment, 2, 2);
    GridPane.setConstraints(tightDeployment, 2, 3);
    GridPane.setConstraints(traditionalFleet, 3, 2);
    GridPane.setConstraints(tacticalFleet, 3, 3);

    selectionScreen.getChildren().addAll(spaciousDeployment, tightDeployment, traditionalFleet,
        tacticalFleet);

    Button playButton = new Button("Play");
    playButton.setOnAction(acttion -> {
      String playerOneName = playerOneNameTextField.getText();
      String playerTwoName = playerTwoNameTextField.getText();
      if (!"".equals(playerOneName)) {
        game.setPlayerOneName(playerOneName);
      }
      if (!"".equals(playerTwoName)) {
        game.setPlayerTwoName(playerTwoName);
      }
      Scene deploymentScene = new Scene(DeploymentScene.display(game, window), 340, 440);
      window.setScene(deploymentScene);
    });
    playButton.setMinWidth(50);
    playScreen.getChildren().add(playButton);

    HBox titleBox = new HBox();
    titleBox.getChildren().add(titleText);
    titleBox.setAlignment(Pos.CENTER);

    VBox selectionBox = new VBox();
    selectionBox.getChildren().add(nameScreen);
    selectionBox.getChildren().add(selectionScreen);

    BorderPane screen = new BorderPane();
    screen.setTop(titleBox);
    screen.setCenter(selectionBox);
    screen.setBottom(playScreen);

    return screen;
  }
}
