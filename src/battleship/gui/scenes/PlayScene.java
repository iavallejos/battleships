package battleship.gui.scenes;

import battleship.Battleship;
import battleship.gui.PlayBoard;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * The scene of the battleship game where the players attacks each other.
 * 
 * @author Ignacio Vallejos
 *
 */
public class PlayScene {
  /**
   * Creates a PlayScene for the game.
   * 
   * @param game The game that is been used.
   * @param window The window where the game is running.
   * @return A BordedPane with the Scene.
   */
  public static BorderPane display(Battleship game, Stage window) {
    HBox titleBox = new HBox();

    final Text titleText = new Text("BATTLESHIP");
    titleText.setStyle("-fx-font: 48 Algerian;");
    titleBox.getChildren().add(titleText);
    titleBox.setAlignment(Pos.CENTER);
    BorderPane screen = new BorderPane();
    screen.setTop(titleBox);

    GridPane playScreen = new GridPane();
    playScreen.setPadding(new Insets(0, 10, 10, 10));
    playScreen.setVgap(10);
    playScreen.setHgap(10);

    HBox playerOneBoardNameContainer = new HBox();
    playerOneBoardNameContainer.setMaxHeight(25);
    playerOneBoardNameContainer.setMaxHeight(25);
    final Text playerOneBoardName = new Text(game.getPlayerOneName());
    final Text playerTwoBoardName = new Text(game.getPlayerTwoName());
    playerOneBoardName.setStyle("-fx-font: 20 arial");
    playerTwoBoardName.setStyle("-fx-font: 20 arial");
    HBox playerTwoBoardNameContainer = new HBox();
    playerOneBoardNameContainer.getChildren().add(playerOneBoardName);
    playerTwoBoardNameContainer.getChildren().add(playerTwoBoardName);
    playerOneBoardNameContainer.setAlignment(Pos.CENTER);
    playerTwoBoardNameContainer.setAlignment(Pos.CENTER);

    Text playerTurn = new Text(game.getPlayerOneName() + "\nis attacking");
    playerTurn.setStyle("-fx-font: 20 arial");

    PlayBoard playerOneBoard = new PlayBoard(game.playerOne, true, playerTurn, window);
    PlayBoard playerTwoBoard = new PlayBoard(game.playerTwo, false, playerTurn, window);
    playerOneBoard.setOponent(playerTwoBoard);
    playerTwoBoard.setOponent(playerOneBoard);
    VBox playerOneBox = new VBox();
    VBox playerTwoBox = new VBox();
    playerOneBox.getChildren().addAll(playerOneBoardNameContainer, playerTwoBoard);
    playerTwoBox.getChildren().addAll(playerTwoBoardNameContainer, playerOneBoard);
    GridPane.setConstraints(playerOneBox, 1, 1);
    GridPane.setConstraints(playerTwoBox, 3, 1);

    HBox playerTurnContainer = new HBox();
    playerTurnContainer.getChildren().add(playerTurn);
    playerTurnContainer.setAlignment(Pos.CENTER);
    GridPane.setConstraints(playerTurnContainer, 2, 1);

    playScreen.getChildren().addAll(playerOneBox, playerTurnContainer, playerTwoBox);
    screen.setCenter(playScreen);

    GridPane resetBox = new GridPane();
    resetBox.setPadding(new Insets(0, 10, 10, 350));
    Button restartButton = new Button("Restart");
    restartButton.setMinWidth(80);
    restartButton.setOnAction(action -> {
      Scene scene = new Scene(LogInScene.display(new Battleship(), window), 320, 270);
      window.setScene(scene);
    });
    resetBox.getChildren().add(restartButton);
    screen.setBottom(resetBox);

    return screen;
  }
}
