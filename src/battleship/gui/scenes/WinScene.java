package battleship.gui.scenes;

import battleship.Battleship;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * The scene of the battleship game where the name of the winner is displayed.
 * 
 * @author Ignacio Vallejos.
 *
 */
public class WinScene {
  /**
   * Creates a PlayScene for the game.
   * 
   * @param winner a String with the name of the winner player.
   * @param window The window where the game is running.
   * @return A BordedPane with the Scene.
   */
  public static BorderPane display(String winner, Stage window) {
    HBox titleBox = new HBox();


    final Text titleText = new Text("BATTLESHIP");
    titleText.setStyle("-fx-font: 48 Algerian;");
    titleBox.getChildren().add(titleText);
    titleBox.setAlignment(Pos.CENTER);

    BorderPane screen = new BorderPane();
    screen.setTop(titleBox);

    final Text congratsText = new Text("Congratulations");
    final Text playerText = new Text(winner);
    final Text winText = new Text("you win!!!!!");
    congratsText.setStyle("-fx-font: 38 Times_New_Roman;");
    playerText.setStyle("-fx-font: 38 Times_New_Roman;");
    winText.setStyle("-fx-font: 38 Times_New_Roman;");

    HBox congratulationBox = new HBox();
    HBox playerBox = new HBox();
    HBox winnerBox = new HBox();

    congratulationBox.getChildren().add(congratsText);
    playerBox.getChildren().add(playerText);
    winnerBox.getChildren().add(winText);

    congratulationBox.setAlignment(Pos.CENTER);
    playerBox.setAlignment(Pos.CENTER);
    winnerBox.setAlignment(Pos.CENTER);

    VBox textBox = new VBox();
    textBox.getChildren().add(congratulationBox);
    textBox.getChildren().add(playerBox);
    textBox.getChildren().add(winnerBox);

    screen.setCenter(textBox);

    GridPane resetBox = new GridPane();
    resetBox.setPadding(new Insets(0, 10, 10, 160));
    Button restartButton = new Button("Restart");
    restartButton.setMinWidth(80);
    restartButton.setOnAction(action -> {
      Scene scene = new Scene(LogInScene.display(new Battleship(), window), 320, 270);
      window.setScene(scene);
    });
    resetBox.getChildren().add(restartButton);
    screen.setBottom(resetBox);

    return screen;
  }

}
