package battleship.ship;

/**
 * Implementing this interface allows an user to get the length, the name of an object and know and
 * set the anchored status of the object.
 * 
 * @author Ignacio Vallejos.
 *
 */
public interface IShip {
  /**
   * Getter for the length.
   * 
   * @return The length of the ship.
   */
  int getLength();

  /**
   * Getter for the name.
   * 
   * @return The name of the ship.
   */
  String getName();

  /**
   * @return If the Object is a Ship or an NullShip.
   */
  boolean isShip();

  /**
   * @return The anchored status of the ship.
   */
  boolean isAnchored();

  /**
   * The ships loses 1 life.
   */
  void attacked();

  /**
   * @return true if lives==0.
   */
  boolean isSinked();

  /**
   * Sets the anchored state of the ship to false.
   */
  void deploy();

}
