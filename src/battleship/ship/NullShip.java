package battleship.ship;

/**
 * The NullShip class implements the interface IShip and its used as a null variable.
 * 
 * @author Ignacio Vallejos.
 */
public class NullShip implements IShip {

  @Override
  public int getLength() {
    return 0;
  }

  @Override
  public String getName() {
    return "";
  }

  @Override
  public boolean isShip() {
    return false;
  }

  @Override
  public boolean isAnchored() {
    return false;
  }

  @Override
  public void attacked() {
    isAnchored();
  }

  @Override
  public boolean isSinked() {
    return false;
  }

  @Override
  public int hashCode() {
    return 1;
  }

  @Override
  public boolean equals(Object anObject) {
    return anObject instanceof NullShip;
  }

  @Override
  public void deploy() {
    isAnchored();
  }
}
