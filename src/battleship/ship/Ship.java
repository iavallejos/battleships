package battleship.ship;

/**
 * The Ship class implements the interface IShip and its used to store the data of a ship: its name,
 * size, if the ship is anchored or not and the lives of the ship(how many times can be attacked
 * before sinking).
 * 
 * @author Ignacio Vallejos.
 */
public class Ship implements IShip {
  private final int length;
  private final String name;
  private boolean anchored;
  private int lives;

  /**
   * Constructor for the class Ship, a ship contains a name, a size, a number of lives and a
   * anchored variable, which can be true or false and indicates if the ship has been deployed or
   * not once created the only thing that can change is the value of anchored, and the remaining
   * lives, the ship is initialized with it's anchored variable set to true and with as many lives
   * as the length of the ship.
   * 
   * @param name represents the name of the ship, it can't be an empty string.
   * @param length represents the size of the ship, it can't 0 or negative.
   */

  public Ship(String name, int length) {
    this.length = length;
    this.name = name;
    this.anchored = true;
    this.lives = length;
  }

  @Override
  public int getLength() {
    return this.length;
  }

  @Override
  public String getName() {
    return this.name;
  }

  @Override
  public boolean isShip() {
    return true;
  }

  @Override
  public boolean isAnchored() {
    return anchored;
  }

  @Override
  public void deploy() {
    this.anchored = false;
  }

  @Override
  public void attacked() {
    lives--;
  }

  @Override
  public boolean isSinked() {
    return lives == 0;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + (anchored ? 1231 : 1237);
    result = prime * result + length;
    result = prime * result + lives;
    result = prime * result + name.hashCode();
    return result;
  }

  @Override
  public boolean equals(Object anObject) {
    return anObject instanceof Ship && ((Ship) anObject).getLength() == this.getLength()
        && ((Ship) anObject).getName() == this.getName()
        && ((Ship) anObject).isAnchored() == this.isAnchored()
        && ((Ship) anObject).lives == this.lives;
  }
}
