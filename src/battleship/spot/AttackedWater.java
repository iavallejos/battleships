package battleship.spot;

import battleship.display.IDisplay;
import javafx.scene.paint.Color;

/**
 * This state its an Spot with water.
 * 
 * @author Ignacio Vallejos.
 *
 */
public class AttackedWater extends State {
  public AttackedWater() {
    this.color = Color.DODGERBLUE;
  }

  @Override
  String display(IDisplay display) {
    return display.attackedWaterSymbol(this);
  }

  @Override
  public int hashCode() {
    return 1;
  }

  @Override
  public boolean equals(Object anObject) {
    return anObject instanceof AttackedWater;
  }
}
