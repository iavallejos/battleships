package battleship.spot;

import battleship.display.IDisplay;
import battleship.ship.IShip;

/**
 * Implementing this interface allows the user to set an State, deploy an IShip, attack the ISpot
 * and to draw it.
 * 
 * @author Ignacio Vallejos.
 *
 */
public interface ISpot {
  /**
   * @param state The State to be established.
   */
  void setState(State state);

  /**
   * Deploys an IShip in the spot, if the state is Water it changes it to ShipState.
   * 
   * @param ship The ship to be deployed in the spot.
   */
  void deployShip(IShip ship);

  /**
   * @return true if there is an unattacked ship part in the Spot, false if not.
   */
  boolean hasShip();

  /**
   * @return true if there is an attacked ship part in the Spot, false if not.
   */
  boolean hasAttackedShip();

  /**
   * Attacks the Spot, if the state is Water it changes it to AtackedWater, and if it is ShipState
   * it changes it to AttackedShip.
   * 
   * @return true if the attacked ISpot has an ShipState State.
   */
  boolean attack();

  /**
   * Method used by the IDisplay interface to get the character representation of the spot.
   * 
   * @param display The Display type that will get the character.
   * @return The character of the spot.
   */
  String display(IDisplay display);

  /**
   * @return The State of the spot.
   */
  State getState();
}
