package battleship.spot;

import battleship.display.IDisplay;
import battleship.ship.IShip;
import javafx.scene.paint.Color;

/**
 * This state its an Spot with a deployed ship.
 * 
 * @author Ignacio Vallejos.
 *
 */
public class ShipState extends State {
  private final IShip ship;

  /**
   * Constructor for the ShipState State, it sets the ships that its deployed on it.
   * 
   * @param ship The ship to be associated to the State.
   */
  public ShipState(IShip ship) {
    this.ship = ship;
    this.color = Color.GRAY;
  }

  @Override
  public boolean hasShip() {
    return true;
  }

  @Override
  boolean attack() {
    this.changeState(new AttackedShip(ship));
    ship.attacked();
    return true;
  }

  @Override
  String display(IDisplay display) {
    return display.shipSymbol(this);
  }

  @Override
  public int hashCode() {
    return 1;
  }

  @Override
  public boolean equals(Object anObject) {
    return anObject instanceof ShipState && ((ShipState) anObject).ship.equals(this.ship);
  }

  @Override
  public IShip getShip() {
    return this.ship;
  }
}
