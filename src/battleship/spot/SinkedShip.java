package battleship.spot;

import battleship.display.IDisplay;
import battleship.ship.IShip;
import javafx.scene.paint.Color;

/**
 * This state its an Spot with a deployed ship that has been sinked.
 * 
 * @author Ignacio Vallejos.
 *
 */
public class SinkedShip extends State {
  private final IShip ship;

  /**
   * Constructor for the AttackedShip State, it sets the ships that its deployed on it.
   * 
   * @param ship The ship to be associated to the State.
   */
  public SinkedShip(IShip ship) {
    this.ship = ship;
    this.color = Color.DARKRED;
  }

  @Override
  public boolean hasShip() {
    return true;
  }

  @Override
  public boolean hasAttackedShip() {
    return true;
  }

  @Override
  String display(IDisplay display) {
    return display.sinkedShipSymbol(this);
  }

  @Override
  public int hashCode() {
    return 1;
  }

  @Override
  public boolean equals(Object anObject) {
    return anObject instanceof SinkedShip && ((SinkedShip) anObject).ship.equals(this.ship);
  }

  @Override
  public IShip getShip() {
    return this.ship;
  }

}
