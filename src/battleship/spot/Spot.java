package battleship.spot;

import battleship.display.IDisplay;
import battleship.ship.IShip;

import java.util.Observable;

/**
 * It's used to fill the grid of the battleship board.
 * 
 * @author Ignacio Vallejos.
 *
 */
public class Spot extends Observable implements ISpot {
  private State state;

  /**
   * Constructor for the Spot, it is a water spot by default.
   */
  public Spot() {
    this.setState(new Water());
  }

  @Override
  public void setState(State state) {
    this.state = state;
    this.state.setSpot(this);
    setChanged();
    notifyObservers(state);
  }

  @Override
  public void deployShip(IShip ship) {
    state.deployShip(ship);
  }

  @Override
  public boolean hasShip() {
    return state.hasShip();
  }

  @Override
  public boolean hasAttackedShip() {
    return state.hasAttackedShip();
  }

  @Override
  public boolean attack() {
    return state.attack();
  }

  @Override
  public String display(IDisplay display) {
    return state.display(display);
  }

  @Override
  public State getState() {
    return this.state;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + state.hashCode();
    return result;
  }

  @Override
  public boolean equals(Object anObject) {
    return anObject instanceof Spot && ((Spot) anObject).getState().equals(this.getState());
  }

}
