package battleship.spot;

import battleship.display.IDisplay;
import battleship.ship.IShip;
import battleship.ship.NullShip;
import javafx.scene.paint.Color;

/**
 * Abstract Class used for the states of the ISpots, it is linked to a ISpot.
 * 
 * @author Ignacio Vallejos.
 *
 */
public abstract class State {
  protected ISpot spot;
  Color color;

  /**
   * Links a spot to the state.
   * 
   * @param spot The spot to be linked.
   */
  public void setSpot(ISpot spot) {
    this.spot = spot;
  }

  /**
   * @param state The new State to be set.
   */
  protected void changeState(State state) {
    spot.setState(state);
  }

  /**
   * Deploys a Ship in the spot.
   * 
   * @param ship The ship to be deployed.
   */
  void deployShip(IShip ship) {
    spot.getClass();
  }


  /**
   * Attacks the spot.
   * 
   * @return true if is an ShipState.
   */
  boolean attack() {
    return false;
  }

  /**
   * @param display The Display mode used to get the character.
   * @return The character that represents the spot.
   */
  abstract String display(IDisplay display);

  /**
   * @return true if there is an unattacked ship part in the Spot, false if not.
   */
  public boolean hasShip() {
    return false;
  }

  /**
   * @return true if there is an attacked ship part in the Spot, false if not.
   */
  public boolean hasAttackedShip() {
    return false;
  }

  /**
   * @return The color of the State.
   */
  public Color getColor() {
    return this.color;
  }

  public void sink() {
    getClass();
  }

  public IShip getShip() {
    return new NullShip();
  }
}
