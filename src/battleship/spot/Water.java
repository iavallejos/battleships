package battleship.spot;

import battleship.display.IDisplay;
import battleship.ship.IShip;
import javafx.scene.paint.Color;

/**
 * This state its an Spot with water that has been attacked.
 * 
 * @author Ignacio Vallejos.
 *
 */
public class Water extends State {
  public Water() {
    this.color = Color.BLUE;
  }

  @Override
  boolean attack() {
    this.changeState(new AttackedWater());
    return false;
  }

  @Override
  void deployShip(IShip ship) {
    this.changeState(new ShipState(ship));
  }

  @Override
  String display(IDisplay display) {
    return display.waterSymbol(this);
  }

  @Override
  public int hashCode() {
    return 1;
  }

  @Override
  public boolean equals(Object anObject) {
    return anObject instanceof Water;
  }
}
