package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import battleship.Battleship;
import battleship.Fleet;
import battleship.Territory;
import battleship.attack.IAttackResult;
import battleship.display.SpaceDisplay;
import battleship.display.TildeDisplay;
import battleship.ship.*;

public class BattleshipTest {

  private Battleship battleship;
  private Battleship battleship2;
  Fleet fleet;


  @Before
  public void fixtureInit() {
    battleship = new Battleship();
    battleship2 = new Battleship();
    fleet = new Fleet();
  }

  @Test
  public void constructor() {
    assertNotNull(battleship);
    assertEquals(battleship, battleship);
    assertEquals(battleship.hashCode(), battleship.hashCode());
    assertEquals(new Battleship(), battleship);
    assertEquals((new Battleship()).hashCode(), battleship.hashCode());
    assertEquals(10, battleship.getTerritoryHeight());
    assertEquals(10, battleship.getTerritoryWidth());
    assertEquals(battleship, battleship2);
    battleship2.useSpaceDisplay();
    assertNotEquals(battleship, battleship2);

    battleship.useSpaceDisplay();
    battleship.useCompactDisplay();
    assertNotEquals(battleship, battleship2);
    assertEquals(battleship.hashCode(), battleship2.hashCode());

    battleship2.useCompactDisplay();
    assertEquals(battleship, battleship2);
    assertEquals(battleship.hashCode(), battleship2.hashCode());
    
    battleship.useReverseDesktopDisplay();
    assertNotEquals(battleship, battleship2);
    assertEquals(battleship.hashCode(), battleship2.hashCode());
    battleship2.useReverseDesktopDisplay();
    assertEquals(battleship, battleship2);
    assertEquals(battleship.hashCode(), battleship2.hashCode());

    assertEquals("a player", battleship.getPlayerOneName());
    assertEquals("a player", battleship.getPlayerTwoName());

    battleship.setPlayerOneName("pepo");
    battleship.setPlayerTwoName("pepa");
    assertEquals("pepo", battleship.getPlayerOneName());
    assertEquals("pepa", battleship.getPlayerTwoName());
  }

  @Test
  public void setTerritorySize() {
    battleship.setTerritorySize(20, 20);

    assertEquals(20, battleship.getTerritoryHeight());
    assertEquals(20, battleship.getTerritoryWidth());
  }

  @Test
  public void useTraditionalFleetTest() {
    assertEquals(fleet.getSize(), battleship.getFleetSize());
    assertEquals(fleet.getSize(), battleship2.getFleetSize());

    IShip ship = fleet.getAnchoredShip();
    ship.deploy();
    IShip expected = ship;

    assertEquals(expected, battleship.playerOneDeployHorizontally(5, 5));
    assertEquals(expected, battleship2.playerTwoDeployVertically(5, 5));

    battleship.useTraditionalFleet();
    battleship2.useTraditionalFleet();
    fleet.setTraditional();

    assertEquals(fleet.getSize(), battleship.getFleetSize());
    assertEquals(fleet.getSize(), battleship2.getFleetSize());

    ship = fleet.getAnchoredShip();
    ship.deploy();
    expected = ship;

    assertEquals(expected, battleship.playerOneDeployHorizontally(5, 3));
    assertEquals(expected, battleship2.playerTwoDeployVertically(3, 5));
  }

  @Test
  public void useTacticalFleet() {
    fleet.setTactical();
    battleship.useTacticalFleet();
    battleship2.useTacticalFleet();

    IShip ship = fleet.getAnchoredShip();
    ship.deploy();
    IShip expected = ship;

    assertEquals(expected, battleship.playerOneDeployHorizontally(5, 5));
    assertEquals(expected, battleship2.playerTwoDeployVertically(5, 5));

    assertEquals(battleship.getFleetSize() - 1, battleship.playerOneNumberOfAnchoredShips());
    assertEquals(1, battleship.playerOneNumberOfDeployedShips());
    assertEquals(battleship.getFleetSize(), battleship.playerTwoNumberOfAnchoredShips());
    assertEquals(1, battleship2.playerTwoNumberOfDeployedShips());
  }

  @Test
  public void useCustomFleet() {
    battleship.useCustomFleet();
    battleship2.useCustomFleet();
    fleet.setCustom();

    assertEquals(new NullShip(), battleship.playerOneDeployHorizontally(5, 5));
    assertEquals(new NullShip(), battleship2.playerOneDeployVertically(5, 5));
    // Add the same ships to fleet and game, then deploy them.
    fleet.addShip("Test Boat 1", 2);
    fleet.addShip("Test Boat 2", 3);
    fleet.addShip("Test Boat 3", 2);
    battleship.addShip("Test Boat 1", 2);
    battleship.addShip("Test Boat 2", 3);
    battleship.addShip("Test Boat 3", 2);

    IShip ship = fleet.getAnchoredShip();
    ship.deploy();
    IShip expected = ship;
    assertEquals(expected, battleship.playerOneDeployHorizontally(5, 5));
    assertTrue(battleship.isPlayerOneShipAt(5, 5));
    assertTrue(battleship.isPlayerOneShipAt(6, 5));
    assertFalse(battleship.isPlayerOneShipAt(7, 5));

    ship = fleet.getAnchoredShip();
    ship.deploy();
    expected = ship;
    assertEquals(new NullShip(), battleship.playerOneDeployHorizontally(5, 45));
    assertEquals(new NullShip(), battleship.playerOneDeployVertically(12, 1));
    assertEquals(expected, battleship.playerOneDeployVertically(3, 6));
    assertTrue(battleship.isPlayerOneShipAt(3, 6));
    assertTrue(battleship.isPlayerOneShipAt(3, 7));
    assertTrue(battleship.isPlayerOneShipAt(3, 8));
    assertFalse(battleship.isPlayerOneShipAt(3, 5));
    assertFalse(battleship.isPlayerOneShipAt(3, 9));

    ship = new Ship("Test Boat 1", 2);
    ship.deploy();
    expected = ship;
    assertEquals(expected, battleship.playerTwoDeployHorizontally(7, 9));
    assertTrue(battleship.isPlayerTwoShipAt(7, 9));
    assertTrue(battleship.isPlayerTwoShipAt(8, 9));
    assertFalse(battleship.isPlayerTwoShipAt(6, 9));
    assertFalse(battleship.isPlayerTwoShipAt(9, 9));
  }

  @Test
  public void displayTerritory() {
    assertEquals(new TildeDisplay().display(new Territory()),
        battleship.displayPlayerOneTerritory());
    battleship.useSpaceDisplay();
    assertEquals(new SpaceDisplay().display(new Territory()),
        battleship.displayPlayerTwoTerritory());
    battleship.useTildeDisplay();
    assertEquals(new TildeDisplay().display(new Territory()),
        battleship.displayPlayerTwoTerritory());
  }

  @Test
  public void desktopDisplayTest() {
    battleship.playerOneDeployVertically(3, 4);
    battleship.playerTwoDeployVertically(5, 6);
    IAttackResult result = battleship.playerOneAttackAt(5, 6);
    assertTrue(result.isAffectedTarget());
    assertFalse(result.isMissedTarget());
    result = battleship.playerOneAttackAt(5, 5);
    assertTrue(result.isMissedTarget());
    assertFalse(result.isAffectedTarget());

    battleship.playerTwoAttackAt(3, 4);
    battleship.playerTwoAttackAt(3, 3);

    assertEquals(expectedPlayerOneNormalDesktop(), battleship.displayPlayerOneDesk());
    assertEquals(expectedPlayerTwoNormalDesktop(), battleship.displayPlayerTwoDesk());

    battleship.useReverseDesktopDisplay();
    assertEquals(expectedPlayerOneReverseDesktop(), battleship.displayPlayerOneDesk());

    battleship.useCompactDisplay();
    assertEquals(expectedPlayerOneCompactDesktop(), battleship.displayPlayerOneDesk());

    battleship.useDesktopDisplay();
    assertEquals(expectedPlayerOneNormalDesktop(), battleship.displayPlayerOneDesk());
  }

  @Test
  public void IteratorTest() {
    assertEquals(fleet.iterator().next(), battleship.getFleetIterator().next());
  }

  @Test
  public void DeploymentTest() {
    battleship.useTightDeployment();
    battleship2.useSpaciousDeployment();

    assertEquals(new NullShip(), battleship.playerOneDeployHorizontally(9, 4));
    assertEquals(new NullShip(), battleship2.playerOneDeployHorizontally(9, 4));

    assertNotEquals(new NullShip(), battleship.playerOneDeployHorizontally(4, 6));
    assertNotEquals(new NullShip(), battleship2.playerOneDeployHorizontally(4, 6));

    assertEquals(new NullShip(), battleship.playerOneDeployVertically(5, 3));
    assertNotEquals(new NullShip(), battleship.playerOneDeployVertically(5, 2));

    assertEquals(new NullShip(), battleship2.playerOneDeployVertically(5, 3));
    assertNotEquals(new NullShip(), battleship2.playerOneDeployVertically(5, 1));
    assertNotEquals(new NullShip(), battleship2.playerOneDeployHorizontally(1, 10));

    assertEquals(new NullShip(), battleship.playerTwoDeployHorizontally(9, 4));
    assertEquals(new NullShip(), battleship2.playerTwoDeployHorizontally(9, 4));

    assertNotEquals(new NullShip(), battleship.playerTwoDeployHorizontally(4, 6));
    assertNotEquals(new NullShip(), battleship2.playerTwoDeployHorizontally(4, 6));

    assertEquals(new NullShip(), battleship.playerTwoDeployVertically(5, 3));
    assertNotEquals(new NullShip(), battleship.playerTwoDeployVertically(5, 2));

    assertEquals(new NullShip(), battleship2.playerTwoDeployVertically(5, 3));
    assertNotEquals(new NullShip(), battleship2.playerTwoDeployVertically(5, 1));
    assertNotEquals(new NullShip(), battleship2.playerTwoDeployHorizontally(1, 10));
  }

  private String expectedPlayerOneNormalDesktop() {
    StringBuilder stream = new StringBuilder();
    stream.append("Player: a player");
    stream.append(System.lineSeparator());
    stream.append(" with 1 out of 5 affected ship parts.");
    stream.append(System.lineSeparator());
    stream.append(System.lineSeparator());
    stream.append("   ABCDEFGHIJ");
    stream.append(System.lineSeparator());
    stream.append(" 1 ~~~~~~~~~~");
    stream.append(System.lineSeparator());
    stream.append(" 2 ~~~~~~~~~~");
    stream.append(System.lineSeparator());
    stream.append(" 3 ~~.~~~~~~~");
    stream.append(System.lineSeparator());
    stream.append(" 4 ~~+~~~~~~~");
    stream.append(System.lineSeparator());
    stream.append(" 5 ~~o~~~~~~~");
    stream.append(System.lineSeparator());
    stream.append(" 6 ~~o~~~~~~~");
    stream.append(System.lineSeparator());
    stream.append(" 7 ~~o~~~~~~~");
    stream.append(System.lineSeparator());
    stream.append(" 8 ~~o~~~~~~~");
    stream.append(System.lineSeparator());
    stream.append(" 9 ~~~~~~~~~~");
    stream.append(System.lineSeparator());
    stream.append("10 ~~~~~~~~~~");
    stream.append(System.lineSeparator());
    stream.append(System.lineSeparator());
    stream.append("Opponent player: a player");
    stream.append(System.lineSeparator());
    stream.append(" with 1 out of 5 affected ship parts.");
    stream.append(System.lineSeparator());
    stream.append(System.lineSeparator());
    stream.append("   ABCDEFGHIJ");
    stream.append(System.lineSeparator());
    stream.append(" 1           ");
    stream.append(System.lineSeparator());
    stream.append(" 2           ");
    stream.append(System.lineSeparator());
    stream.append(" 3           ");
    stream.append(System.lineSeparator());
    stream.append(" 4           ");
    stream.append(System.lineSeparator());
    stream.append(" 5     /     ");
    stream.append(System.lineSeparator());
    stream.append(" 6     x     ");
    stream.append(System.lineSeparator());
    stream.append(" 7           ");
    stream.append(System.lineSeparator());
    stream.append(" 8           ");
    stream.append(System.lineSeparator());
    stream.append(" 9           ");
    stream.append(System.lineSeparator());
    stream.append("10           ");
    stream.append(System.lineSeparator());

    return stream.toString();
  }

  private String expectedPlayerTwoNormalDesktop() {
    StringBuilder stream = new StringBuilder();

    stream.append("Player: a player");
    stream.append(System.lineSeparator());
    stream.append(" with 1 out of 5 affected ship parts.");
    stream.append(System.lineSeparator());
    stream.append(System.lineSeparator());
    stream.append("   ABCDEFGHIJ");
    stream.append(System.lineSeparator());
    stream.append(" 1 ~~~~~~~~~~");
    stream.append(System.lineSeparator());
    stream.append(" 2 ~~~~~~~~~~");
    stream.append(System.lineSeparator());
    stream.append(" 3 ~~~~~~~~~~");
    stream.append(System.lineSeparator());
    stream.append(" 4 ~~~~~~~~~~");
    stream.append(System.lineSeparator());
    stream.append(" 5 ~~~~.~~~~~");
    stream.append(System.lineSeparator());
    stream.append(" 6 ~~~~+~~~~~");
    stream.append(System.lineSeparator());
    stream.append(" 7 ~~~~o~~~~~");
    stream.append(System.lineSeparator());
    stream.append(" 8 ~~~~o~~~~~");
    stream.append(System.lineSeparator());
    stream.append(" 9 ~~~~o~~~~~");
    stream.append(System.lineSeparator());
    stream.append("10 ~~~~o~~~~~");
    stream.append(System.lineSeparator());
    stream.append(System.lineSeparator());
    stream.append("Opponent player: a player");
    stream.append(System.lineSeparator());
    stream.append(" with 1 out of 5 affected ship parts.");
    stream.append(System.lineSeparator());
    stream.append(System.lineSeparator());
    stream.append("   ABCDEFGHIJ");
    stream.append(System.lineSeparator());
    stream.append(" 1           ");
    stream.append(System.lineSeparator());
    stream.append(" 2           ");
    stream.append(System.lineSeparator());
    stream.append(" 3   /       ");
    stream.append(System.lineSeparator());
    stream.append(" 4   x       ");
    stream.append(System.lineSeparator());
    stream.append(" 5           ");
    stream.append(System.lineSeparator());
    stream.append(" 6           ");
    stream.append(System.lineSeparator());
    stream.append(" 7           ");
    stream.append(System.lineSeparator());
    stream.append(" 8           ");
    stream.append(System.lineSeparator());
    stream.append(" 9           ");
    stream.append(System.lineSeparator());
    stream.append("10           ");
    stream.append(System.lineSeparator());

    return stream.toString();
  }

  private String expectedPlayerOneReverseDesktop() {
    StringBuilder stream = new StringBuilder();

    stream.append("Player: a player");
    stream.append(System.lineSeparator());
    stream.append(" with 1 out of 5 affected ship parts.");
    stream.append(System.lineSeparator());
    stream.append(System.lineSeparator());
    stream.append("           1");
    stream.append(System.lineSeparator());
    stream.append("  1234567890");
    stream.append(System.lineSeparator());
    stream.append("A ~~~~~~~~~~");
    stream.append(System.lineSeparator());
    stream.append("B ~~~~~~~~~~");
    stream.append(System.lineSeparator());
    stream.append("C ~~.~~~~~~~");
    stream.append(System.lineSeparator());
    stream.append("D ~~+~~~~~~~");
    stream.append(System.lineSeparator());
    stream.append("E ~~o~~~~~~~");
    stream.append(System.lineSeparator());
    stream.append("F ~~o~~~~~~~");
    stream.append(System.lineSeparator());
    stream.append("G ~~o~~~~~~~");
    stream.append(System.lineSeparator());
    stream.append("H ~~o~~~~~~~");
    stream.append(System.lineSeparator());
    stream.append("I ~~~~~~~~~~");
    stream.append(System.lineSeparator());
    stream.append("J ~~~~~~~~~~");
    stream.append(System.lineSeparator());
    stream.append(System.lineSeparator());
    stream.append("Opponent player: a player");
    stream.append(System.lineSeparator());
    stream.append(" with 1 out of 5 affected ship parts.");
    stream.append(System.lineSeparator());
    stream.append(System.lineSeparator());
    stream.append("           1");
    stream.append(System.lineSeparator());
    stream.append("  1234567890");
    stream.append(System.lineSeparator());
    stream.append("A           ");
    stream.append(System.lineSeparator());
    stream.append("B           ");
    stream.append(System.lineSeparator());
    stream.append("C           ");
    stream.append(System.lineSeparator());
    stream.append("D           ");
    stream.append(System.lineSeparator());
    stream.append("E     /     ");
    stream.append(System.lineSeparator());
    stream.append("F     x     ");
    stream.append(System.lineSeparator());
    stream.append("G           ");
    stream.append(System.lineSeparator());
    stream.append("H           ");
    stream.append(System.lineSeparator());
    stream.append("I           ");
    stream.append(System.lineSeparator());
    stream.append("J           ");
    stream.append(System.lineSeparator());

    return stream.toString();
  }

  private String expectedPlayerOneCompactDesktop() {
    StringBuilder stream = new StringBuilder();

    stream.append("Player: a player");
    stream.append(System.lineSeparator());
    stream.append(" with 1 out of 5 affected ship parts.");
    stream.append(System.lineSeparator());
    stream.append(System.lineSeparator());
    stream.append("~~~~~~~~~~");
    stream.append(System.lineSeparator());
    stream.append("~~~~~~~~~~");
    stream.append(System.lineSeparator());
    stream.append("~~.~~~~~~~");
    stream.append(System.lineSeparator());
    stream.append("~~+~~~~~~~");
    stream.append(System.lineSeparator());
    stream.append("~~o~~~~~~~");
    stream.append(System.lineSeparator());
    stream.append("~~o~~~~~~~");
    stream.append(System.lineSeparator());
    stream.append("~~o~~~~~~~");
    stream.append(System.lineSeparator());
    stream.append("~~o~~~~~~~");
    stream.append(System.lineSeparator());
    stream.append("~~~~~~~~~~");
    stream.append(System.lineSeparator());
    stream.append("~~~~~~~~~~");
    stream.append(System.lineSeparator());
    stream.append(System.lineSeparator());
    stream.append("Opponent player: a player");
    stream.append(System.lineSeparator());
    stream.append(" with 1 out of 5 affected ship parts.");
    stream.append(System.lineSeparator());
    stream.append(System.lineSeparator());
    stream.append("          ");
    stream.append(System.lineSeparator());
    stream.append("          ");
    stream.append(System.lineSeparator());
    stream.append("          ");
    stream.append(System.lineSeparator());
    stream.append("          ");
    stream.append(System.lineSeparator());
    stream.append("    /     ");
    stream.append(System.lineSeparator());
    stream.append("    x     ");
    stream.append(System.lineSeparator());
    stream.append("          ");
    stream.append(System.lineSeparator());
    stream.append("          ");
    stream.append(System.lineSeparator());
    stream.append("          ");
    stream.append(System.lineSeparator());
    stream.append("          ");
    stream.append(System.lineSeparator());

    return stream.toString();
  }
}
