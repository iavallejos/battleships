package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import battleship.*;
import battleship.ship.*;


public class DeploymentTest {

  private Territory territory;
  private IDeployment spacious;
  private IDeployment tight;
  private IShip ship1;

  @Before
  public void fixtureInit() {
    territory = new Territory();
    spacious = new SpaciousDeployment();
    tight = new TightDeployment();
    ship1 = new Ship("Test1", 3);
  }

  @Test
  public void ConstructorTest() {
    assertNotNull(spacious);
    assertEquals(new SpaciousDeployment(), spacious);
    assertEquals(new SpaciousDeployment().hashCode(), spacious.hashCode());
    assertNotNull(tight);
    assertEquals(new TightDeployment(), tight);
    assertEquals(new TightDeployment().hashCode(), tight.hashCode());
  }

  @Test
  public void Deployable() {
    assertFalse(tight.deployable(9, 4, 11, 4, territory));
    assertFalse(spacious.deployable(9, 4, 11, 4, territory));

    assertFalse(tight.deployable(4, 4, 6, 6, territory));
    assertFalse(spacious.deployable(4, 4, 6, 6, territory));

    assertTrue(tight.deployable(4, 4, 7, 4, territory));
    assertTrue(spacious.deployable(4, 4, 7, 4, territory));

    territory.deployShip(4, 4, 7, 4, ship1);

    assertFalse(tight.deployable(4, 4, 7, 4, territory));
    assertFalse(spacious.deployable(4, 4, 7, 4, territory));

    assertTrue(tight.deployable(4, 3, 7, 3, territory));
    assertFalse(spacious.deployable(4, 3, 7, 3, territory));
    assertTrue(spacious.deployable(4, 2, 7, 2, territory));
  }

}
