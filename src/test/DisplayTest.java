package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import battleship.HorizontalPosition;
import battleship.IDeployment;
import battleship.Territory;
import battleship.TightDeployment;
import battleship.VerticalPosition;
import battleship.display.IDisplay;
import battleship.display.OponnentDisplay;
import battleship.display.SpaceDisplay;
import battleship.display.TildeDisplay;
import battleship.ship.*;

public class DisplayTest {

  private Territory territory;
  private IShip ship1;
  private IShip ship2;
  private IShip ship3;
  private IDeployment rule;
  private IDisplay space;
  private IDisplay tilde;
  private IDisplay oponent;
  private StringBuilder stream;
  private String spaceExpected;
  private String tildeExpected;

  @Before
  public void fixtureInit() {
    territory = new Territory();
    ship1 = new Ship("Test1", 3);
    ship2 = new Ship("Test2", 4);
    ship3 = new Ship("Test3", 3);
    rule = new TightDeployment();
    space = new SpaceDisplay();
    tilde = new TildeDisplay();
    oponent = new OponnentDisplay();
    stream = new StringBuilder();

    new HorizontalPosition().deploy(5, 6, ship1, territory, rule);
    new VerticalPosition().deploy(3, 4, ship2, territory, rule);
    new HorizontalPosition().deploy(6, 2, ship3, territory, rule);

    stream.append("~~~~~~~~~~");
    stream.append(System.lineSeparator());
    stream.append("~~~~.###~~");
    stream.append(System.lineSeparator());
    stream.append("~~~~~~~~~~");
    stream.append(System.lineSeparator());
    stream.append("~~o~~~~~~~");
    stream.append(System.lineSeparator());
    stream.append("~~+~~~~.~~");
    stream.append(System.lineSeparator());
    stream.append("~~o~ooo~~~");
    stream.append(System.lineSeparator());
    stream.append("~~o~~~~~~~");
    stream.append(System.lineSeparator());
    stream.append("~~~~~~~~~~");
    stream.append(System.lineSeparator());
    stream.append("~~~~~~~~~~");
    stream.append(System.lineSeparator());
    stream.append("~~~~~~~~~~");
    stream.append(System.lineSeparator());
    tildeExpected = stream.toString();

    stream = new StringBuilder();
    stream.append("          ");
    stream.append(System.lineSeparator());
    stream.append("    .###  ");
    stream.append(System.lineSeparator());
    stream.append("          ");
    stream.append(System.lineSeparator());
    stream.append("  *       ");
    stream.append(System.lineSeparator());
    stream.append("  x    .  ");
    stream.append(System.lineSeparator());
    stream.append("  * ***   ");
    stream.append(System.lineSeparator());
    stream.append("  *       ");
    stream.append(System.lineSeparator());
    stream.append("          ");
    stream.append(System.lineSeparator());
    stream.append("          ");
    stream.append(System.lineSeparator());
    stream.append("          ");
    stream.append(System.lineSeparator());
    
    spaceExpected = stream.toString();
  }

  @Test
  public void ConstructorTest() {
    assertNotNull(space);
    assertEquals(new SpaceDisplay(), space);
    assertEquals(new SpaceDisplay().hashCode(), space.hashCode());
    assertNotNull(tilde);
    assertEquals(new TildeDisplay(), tilde);
    assertEquals(new TildeDisplay().hashCode(), tilde.hashCode());
    assertNotNull(oponent);
    assertEquals(new OponnentDisplay(), oponent);
    assertEquals(new OponnentDisplay().hashCode(), oponent.hashCode());
  }

  @Test
  public void Display() {
    territory.getBattlefield()[1][4].attack();
    territory.getBattlefield()[1][5].attack();
    territory.getBattlefield()[1][6].attack();
    territory.getBattlefield()[1][7].attack();
    territory.getBattlefield()[4][7].attack();
    territory.getBattlefield()[4][2].attack();

    assertEquals(spaceExpected, space.display(territory));
    assertEquals(tildeExpected, tilde.display(territory));
  }
}
