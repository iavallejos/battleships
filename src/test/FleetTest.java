package test;

import static org.junit.Assert.*;

import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;

import battleship.Fleet;
import battleship.ship.IShip;
import battleship.ship.NullShip;
import battleship.ship.Ship;

public class FleetTest {

  private Fleet testFleet1;
  private Fleet testFleet2;
  private Fleet testFleet3;
  private LinkedList<IShip> tacticalFleet;
  private LinkedList<IShip> traditionalFleet;

  @Before
  public void fixtureInit() {
    testFleet1 = new Fleet();
    testFleet2 = new Fleet();
    testFleet3 = new Fleet();

    traditionalFleet = new LinkedList<IShip>();
    traditionalFleet.add(new Ship("Aircraft carrier", 5));
    traditionalFleet.add(new Ship("Battleship", 4));
    traditionalFleet.add(new Ship("Submarine", 3));
    traditionalFleet.add(new Ship("Destroyer", 3));
    traditionalFleet.add(new Ship("Patrol boat", 2));

    tacticalFleet = new LinkedList<IShip>();
    tacticalFleet.add(new Ship("Aircraft carrier", 5));
    tacticalFleet.add(new Ship("Battleship", 4));
    tacticalFleet.add(new Ship("Cruiser", 3));
    tacticalFleet.add(new Ship("Destroyer", 2));
    tacticalFleet.add(new Ship("Destroyer", 2));
    tacticalFleet.add(new Ship("Submarine", 1));
    tacticalFleet.add(new Ship("Submarine", 1));
  }

  @Test
  public void setFleet() {
    assertNotNull(testFleet1);
    assertEquals(traditionalFleet, testFleet1.getFleet());
    assertEquals(testFleet1, testFleet1);
    assertEquals(testFleet1.hashCode(), testFleet1.hashCode());
    assertEquals(testFleet1, testFleet2);
    assertEquals(testFleet1.hashCode(), testFleet2.hashCode());
    assertEquals(testFleet1, new Fleet());
    assertEquals(testFleet1.hashCode(), (new Fleet()).hashCode());

    testFleet2.setTactical();
    testFleet3.setCustom();
    assertNotEquals(testFleet1, testFleet2);
    assertNotEquals(testFleet1.hashCode(), testFleet2.hashCode());
    assertNotEquals(testFleet1, testFleet3);
    assertNotEquals(testFleet1.hashCode(), testFleet3.hashCode());
    assertNotEquals(testFleet2, testFleet3);
    assertNotEquals(testFleet2.hashCode(), testFleet3.hashCode());

    testFleet2.setTraditional();
    assertEquals(testFleet1, testFleet2);
    assertEquals(testFleet1.hashCode(), testFleet2.hashCode());
    assertNotEquals(testFleet1, testFleet3);
    assertNotEquals(testFleet1.hashCode(), testFleet3.hashCode());
    assertNotEquals(testFleet2, testFleet3);
    assertNotEquals(testFleet2.hashCode(), testFleet3.hashCode());
  }

  @Test
  public void addToFleetTest() {
    Ship dummyShip = new Ship("dummy", 5);
    assertFalse(testFleet1.getFleet().contains(dummyShip));
    assertEquals(5, testFleet1.getSize());
    assertEquals(5, testFleet1.numberOfAnchoredShips());
    assertEquals(0, testFleet1.numberOfDeployedShips());

    testFleet1.addShip("dummy", 5);
    assertTrue(testFleet1.getFleet().contains(dummyShip));
    assertEquals(6, testFleet1.getSize());
    assertEquals(6, testFleet1.numberOfAnchoredShips());
    assertEquals(0, testFleet1.numberOfDeployedShips());

    IShip expected = testFleet1.getAnchoredShip();
    assertNotEquals(new NullShip(), expected);
    expected.deploy();

    assertEquals(6, testFleet1.getSize());
    assertEquals(5, testFleet1.numberOfAnchoredShips());
    assertEquals(1, testFleet1.numberOfDeployedShips());

    expected = testFleet1.getAnchoredShip();
    assertNotEquals(new NullShip(), expected);
    expected.deploy();

    expected = testFleet1.getAnchoredShip();
    assertNotEquals(new NullShip(), expected);
    expected.deploy();

    expected = testFleet1.getAnchoredShip();
    assertNotEquals(new NullShip(), expected);
    expected.deploy();

    expected = testFleet1.getAnchoredShip();
    assertNotEquals(new NullShip(), expected);
    expected.deploy();

    assertEquals(6, testFleet1.getSize());
    assertEquals(1, testFleet1.numberOfAnchoredShips());
    assertEquals(5, testFleet1.numberOfDeployedShips());

    expected = testFleet1.getAnchoredShip();
    assertNotEquals(new NullShip(), expected);
    expected.deploy();

    assertEquals(6, testFleet1.getSize());
    assertEquals(0, testFleet1.numberOfAnchoredShips());
    assertEquals(6, testFleet1.numberOfDeployedShips());

    assertEquals(new NullShip(), testFleet1.getAnchoredShip());

  }
  
  @Test
  public void IteratorTest(){
    
    assertEquals(traditionalFleet.iterator().next(),testFleet1.iterator().next());
  }
}