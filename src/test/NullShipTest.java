package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import battleship.ship.NullShip;

public class NullShipTest {


  private NullShip ship1;
  private NullShip ship2;

  @Before
  public void fixtureInit() {
    ship1 = new NullShip();
    ship2 = new NullShip();
  }

  @Test
  public void constructor() {
    assertNotNull(ship1);
    assertFalse(ship1.isShip());
    assertEquals(ship1.hashCode(), new NullShip().hashCode());
    assertEquals(ship1, ship2);
    assertEquals("", ship1.getName());
    assertEquals(0, ship1.getLength());
    assertEquals(ship1, ship1);
    assertFalse(ship1.isAnchored());
    assertFalse(ship1.isSinked());
    ship1.attacked();
    assertEquals(ship2, ship1);
    ship1.deploy();
    assertEquals(ship2, ship1);
  }

}
