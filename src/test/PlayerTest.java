package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import battleship.Fleet;
import battleship.Player;
import battleship.SpaciousDeployment;
import battleship.Territory;
import battleship.TightDeployment;
import battleship.display.SpaceDisplay;
import battleship.display.TildeDisplay;
import battleship.ship.IShip;
import battleship.ship.NullShip;

public class PlayerTest {
  private Player player;
  private Player player2;
  Fleet fleet;


  @Before
  public void fixtureInit() {
    player = new Player();
    player2 = new Player();
    fleet = new Fleet();
  }

  @Test
  public void constructor() {
    assertNotNull(player);
    assertEquals(player, player);
    assertEquals(player.hashCode(), player.hashCode());
    assertEquals(new Player(), player);
    assertEquals((new Player()).hashCode(), player.hashCode());
    assertEquals(10, player.getTerritoryHeight());
    assertEquals(10, player.getTerritoryWidth());
    assertEquals(player, player2);
    player2.setDisplay(new SpaceDisplay());
    assertNotEquals(player, player2);
    assertEquals("a player", player.getName());
    player.setName("Test");
    assertEquals("Test", player.getName());
    assertEquals(new Territory(), player.getTerritory());
    assertEquals(new TildeDisplay(), player.getDisplay());
  }

  @Test
  public void setTerritorySize() {
    player.setTerritorySize(20, 20);

    assertEquals(20, player.getTerritoryHeight());
    assertEquals(20, player.getTerritoryWidth());
  }

  @Test
  public void useTraditionalFleetTest() {
    assertEquals(fleet.getSize(), player.getFleetSize());
    assertEquals(fleet.getSize(), player2.getFleetSize());

    IShip ship = fleet.getAnchoredShip();
    ship.deploy();
    IShip expected = ship;

    assertEquals(expected, player.deployHorizontally(5, 5));
    assertEquals(expected, player2.deployVertically(5, 5));

    player.getFleet().setTraditional();
    player2.getFleet().setTraditional();
    fleet.setTraditional();

    assertEquals(fleet.getSize(), player.getFleetSize());
    assertEquals(fleet.getSize(), player2.getFleetSize());

    ship = fleet.getAnchoredShip();
    ship.deploy();
    expected = ship;

    assertEquals(expected, player.deployHorizontally(5, 3));
    assertEquals(expected, player2.deployVertically(3, 5));
  }

  @Test
  public void useTacticalFleet() {
    fleet.setTactical();
    player.getFleet().setTactical();
    player2.getFleet().setTactical();

    IShip ship = fleet.getAnchoredShip();
    ship.deploy();
    IShip expected = ship;

    assertEquals(expected, player.deployHorizontally(5, 5));
    assertEquals(expected, player2.deployVertically(5, 5));

    assertEquals(player.getFleetSize() - 1, player.numberOfAnchoredShips());
    assertEquals(1, player.numberOfDeployedShips());
  }

  @Test
  public void useCustomFleet() {
    player.getFleet().setCustom();
    player2.getFleet().setCustom();
    fleet.setCustom();

    assertEquals(new NullShip(), player.deployHorizontally(5, 5));
    assertEquals(new NullShip(), player2.deployVertically(5, 5));
    // Add the same ships to fleet and game, then deploy them.
    fleet.addShip("Test Boat 1", 2);
    fleet.addShip("Test Boat 2", 3);
    fleet.addShip("Test Boat 3", 2);
    player.addShip("Test Boat 1", 2);
    player.addShip("Test Boat 2", 3);
    player.addShip("Test Boat 3", 2);

    IShip ship = fleet.getAnchoredShip();
    ship.deploy();
    IShip expected = ship;
    assertEquals(expected, player.deployHorizontally(5, 5));
    assertTrue(player.isShipAt(5, 5));
    assertTrue(player.isShipAt(6, 5));
    assertFalse(player.isShipAt(7, 5));

    ship = fleet.getAnchoredShip();
    ship.deploy();
    expected = ship;
    assertEquals(new NullShip(), player.deployHorizontally(5, 45));
    assertEquals(new NullShip(), player.deployVertically(12, 1));
    assertEquals(expected, player.deployVertically(3, 6));
    assertTrue(player.isShipAt(3, 6));
    assertTrue(player.isShipAt(3, 7));
    assertTrue(player.isShipAt(3, 8));
    assertFalse(player.isShipAt(2, 4));
    assertFalse(player.isShipAt(2, 8));

    ship = fleet.getAnchoredShip();
    ship.deploy();
    expected = ship;
    assertEquals(expected, player.deployHorizontally(7, 9));
    assertTrue(player.isShipAt(7, 9));
    assertTrue(player.isShipAt(8, 9));
    assertFalse(player.isShipAt(6, 9));
    assertFalse(player.isShipAt(9, 9));
  }

  @Test
  public void displayTerritory() {
    assertEquals(new TildeDisplay().display(new Territory()), player.displayTerritory());
    assertEquals(new TildeDisplay(), player.getDisplay());
    player.setDisplay(new SpaceDisplay());
    assertEquals(new SpaceDisplay().display(new Territory()), player.displayTerritory());
    assertEquals(new SpaceDisplay(), player.getDisplay());
    player.setDisplay(new TildeDisplay());
    assertEquals(new TildeDisplay().display(new Territory()), player.displayTerritory());
    assertEquals(new TildeDisplay(), player.getDisplay());
  }

  @Test
  public void IteratorTest() {
    assertEquals(fleet.iterator().next(), player.getFleetIterator().next());
  }

  @Test
  public void DeploymentTest() {
    player.setDeployment(new TightDeployment());
    player2.setDeployment(new SpaciousDeployment());

    assertEquals(new NullShip(), player.deployHorizontally(9, 4));
    assertEquals(new NullShip(), player2.deployHorizontally(9, 4));

    assertNotEquals(new NullShip(), player.deployHorizontally(4, 6));
    assertNotEquals(new NullShip(), player2.deployHorizontally(4, 6));

    assertEquals(new NullShip(), player.deployVertically(5, 3));
    assertNotEquals(new NullShip(), player.deployVertically(5, 2));

    assertEquals(new NullShip(), player2.deployVertically(5, 3));
    assertNotEquals(new NullShip(), player2.deployVertically(5, 1));
    assertNotEquals(new NullShip(), player2.deployHorizontally(1, 10));

    assertEquals(9, player.getTerritory().totalShipParts());
    assertEquals(12, player2.getTerritory().totalShipParts());

    assertEquals(0, player.getTerritory().affectedShipParts());
    assertEquals(0, player2.getTerritory().affectedShipParts());

    assertFalse(player.attackedAt(1, 1));
    assertTrue(player.attackedAt(5, 2));

    assertEquals(9, player.getTerritory().totalShipParts());
    assertEquals(1, player.getTerritory().affectedShipParts());

  }
}
