package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import battleship.HorizontalPosition;
import battleship.IDeployment;
import battleship.IPosition;
import battleship.SpaciousDeployment;
import battleship.Territory;
import battleship.TightDeployment;
import battleship.VerticalPosition;
import battleship.ship.*;

public class PositionTest {
  private IShip ship1;
  private IShip ship2;
  private Territory territory1;
  private Territory territory2;
  private IPosition horizontal;
  private IPosition vertical;
  private IDeployment tight;


  @Before
  public void fixtureInit() {
    ship1 = new Ship("Test1", 3);
    ship2 = new Ship("Test2", 2);
    territory1 = new Territory();
    territory2 = new Territory();
    horizontal = new HorizontalPosition();
    vertical = new VerticalPosition();
    tight = new TightDeployment();
  }

  @Test
  public void ConstructorTest() {
    assertNotNull(horizontal);
    assertEquals(new HorizontalPosition(), horizontal);
    assertEquals(new HorizontalPosition().hashCode(), horizontal.hashCode());
    assertNotNull(vertical);
    assertEquals(new VerticalPosition(), vertical);
    assertEquals(new VerticalPosition().hashCode(), vertical.hashCode());
  }

  @Test
  public void DeploymentTest() {
    assertFalse(horizontal.deploy(9, 8, ship1, territory1, tight));
    assertFalse(vertical.deploy(8, 9, ship1, territory2, tight));

    assertFalse(territory1.shipAt(5, 6));
    assertFalse(territory1.shipAt(6, 6));
    assertFalse(territory1.shipAt(7, 6));
    horizontal.deploy(5, 6, ship1, territory1, tight);
    assertTrue(territory1.shipAt(5, 6));
    assertTrue(territory1.shipAt(6, 6));
    assertTrue(territory1.shipAt(7, 6));
    
    assertFalse(vertical.deploy(5, 5, ship2, territory1, tight));
    assertFalse(vertical.deploy(5, 4, ship2, territory1, new SpaciousDeployment()));
    assertTrue(vertical.deploy(5, 4, ship2, territory1, tight));
    
    assertFalse(territory2.shipAt(5, 6));
    assertFalse(territory2.shipAt(5, 7));
    assertFalse(territory2.shipAt(5, 8));
    vertical.deploy(5, 6, ship1, territory2, tight);
    assertTrue(territory2.shipAt(5, 6));
    assertTrue(territory2.shipAt(5, 7));
    assertTrue(territory2.shipAt(5, 8));

    assertFalse(horizontal.deploy(4, 6, ship2, territory2, tight));
    assertFalse(horizontal.deploy(3, 6, ship2, territory2, new SpaciousDeployment()));
    assertTrue(horizontal.deploy(3, 6, ship2, territory2, tight));
  }

}
