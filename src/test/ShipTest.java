package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import battleship.ship.Ship;

public class ShipTest {

  private Ship ship1;
  private Ship ship2;

  @Before
  public void fixtureInit() {
    ship1 = new Ship("test1", 3);
    ship2 = new Ship("test2", 4);
  }

  @Test
  public void constructor() {
    assertNotNull(ship1);
    assertTrue(ship1.isShip());
    assertEquals(ship1.hashCode(), new Ship("test1", 3).hashCode());
    assertEquals("test1", ship1.getName());
    assertEquals(3, ship1.getLength());
    assertEquals(ship1, ship1);
    assertNotEquals(ship1, ship2);
    assertEquals(ship1, new Ship("test1", 3));
    assertTrue(ship1.isAnchored());

    assertFalse(ship1.isSinked());
    ship1.attacked();
    assertFalse(ship1.isSinked());
    ship1.attacked();
    assertFalse(ship1.isSinked());
    ship1.attacked();
    assertTrue(ship1.isSinked());

    Ship ship3 = new Ship("test1", 3);
    assertNotEquals(ship1.hashCode(), ship3.hashCode());

    Ship ship4 = new Ship("test1", 3);
    assertEquals(ship3, ship4);
    assertEquals(ship3.hashCode(), ship4.hashCode());
    ship4.deploy();
    assertNotEquals(ship3, ship4);
    assertNotEquals(ship3.hashCode(), ship4.hashCode());

  }

}
