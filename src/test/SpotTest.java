package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import battleship.ship.IShip;
import battleship.ship.Ship;
import battleship.spot.*;

public class SpotTest {
  private ISpot spot1;
  private ISpot spot2;

  @Before
  public void fixtureInit() {
    spot1 = new Spot();
    spot2 = new Spot();
  }

  @Test
  public void ConstructorTest() {
    assertNotNull(spot1);
    assertEquals(spot1, spot1);
    assertEquals(new Spot(), spot1);
    assertEquals(new Spot().hashCode(), spot1.hashCode());
    assertEquals(spot1, spot2);
    assertEquals(spot1.hashCode(), spot2.hashCode());
    assertEquals(new Water(), spot1.getState());
  }

  @Test
  public void gameTest() {
    assertFalse(spot1.hasShip());
    assertEquals(new Water(), spot1.getState());
    assertFalse(spot1.attack());
    assertEquals(new AttackedWater(), spot1.getState());
    assertEquals(new AttackedWater().hashCode(), spot1.getState().hashCode());
    assertFalse(spot1.hasShip());

    spot2.deployShip(new Ship("Test", 2));
    assertEquals(new ShipState(new Ship("Test", 2)), spot2.getState());
    assertNotEquals(new ShipState(new Ship("Test", 3)), spot2.getState());
    assertEquals(new ShipState(new Ship("Test", 2)).hashCode(), spot2.getState().hashCode());
    assertTrue(spot2.hasShip());

    assertTrue(spot2.attack());

    IShip ship = new Ship("Test", 2);
    ship.attacked();
    assertEquals(new AttackedShip(ship), spot2.getState());
    assertNotEquals(new AttackedShip(new Ship("Test", 2)), spot2.getState());
    assertEquals(new AttackedShip(ship).hashCode(), spot2.getState().hashCode());
    assertTrue(spot2.hasShip());
    
    assertNotEquals(spot1, spot2);
  }

  @Test
  public void emptyTest() {
    assertEquals(spot1, spot2);
    spot1.attack();
    spot2.attack();
    assertEquals(spot1, spot2);
    spot1.attack();
    assertEquals(spot1, spot2);
    spot1.deployShip(new Ship("test", 2));
  }



}
