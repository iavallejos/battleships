package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import battleship.Territory;
import battleship.ship.IShip;
import battleship.ship.Ship;
import battleship.spot.AttackedShip;
import battleship.spot.AttackedWater;
import battleship.spot.ISpot;
import battleship.spot.ShipState;
import battleship.spot.Water;

public class TerritoryTest {


  private Territory testBoard1;
  private Territory testBoard2;

  @Before
  public void fixtureInit() {
    testBoard1 = new Territory();
    testBoard2 = new Territory();
  }

  @Test
  public void reSize() {
    testBoard2.reSize(20, 30);

    assertNotNull(testBoard1);
    assertEquals(10, testBoard1.getHeight());
    assertEquals(10, testBoard1.getWidth());
    assertNotEquals(10, testBoard2.getHeight());
    assertNotEquals(10, testBoard2.getWidth());
    assertEquals(30, testBoard2.getHeight());
    assertEquals(20, testBoard2.getWidth());

    assertEquals(testBoard1, testBoard1);
    assertNotEquals(testBoard1, testBoard2);
    assertEquals(testBoard1, new Territory());

    assertEquals(testBoard1.hashCode(), testBoard1.hashCode());
    assertNotEquals(testBoard1.hashCode(), testBoard2.hashCode());
    assertEquals(testBoard1.hashCode(), (new Territory()).hashCode());
  }

  @Test
  public void initialSettings() {
    for (ISpot[] column : testBoard1.getBattlefield()) {
      for (ISpot square : column) {
        assertEquals(new Water(), square.getState());
      }

    }
  }

  @Test
  public void deployShip() {
    IShip ship1 = new Ship("test", 2);
    assertNotEquals(new ShipState(ship1), testBoard1.getBattlefield()[4][7].getState());
    assertFalse(testBoard1.shipAt(4, 7));
    assertEquals(new Water(), testBoard1.getBattlefield()[4][7].getState());
    testBoard1.deployShip(4, 6, 4, 7, ship1);
    assertEquals(new ShipState(ship1), testBoard1.getBattlefield()[5][3].getState());
    assertTrue(testBoard1.shipAt(4, 6));
    
    assertEquals(new Water(), testBoard1.getBattlefield()[4][4].getState());
    assertFalse(testBoard1.attack(5, 5));
    assertEquals(new AttackedWater(),testBoard1.getBattlefield()[4][4].getState());
    
    assertEquals(new ShipState(ship1), testBoard1.getBattlefield()[6][3].getState());
    assertTrue(testBoard1.attack(4, 7));
    assertEquals(new AttackedShip(ship1), testBoard1.getBattlefield()[6][3].getState());
    
    assertFalse(testBoard1.attack(20, 20));
    
  }
  

}
